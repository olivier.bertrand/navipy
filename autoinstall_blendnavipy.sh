#!/bin/bash

# First install blender
# apt-get install -y -qq blender
# Get version python in blender
blender --background -noaudio --python versionblendpy.py

# Create a virtual environment with a matching pythin version
pip3 install virtualenv
virtualenv -p python$(head -n 1 versionblendpy.txt) venv
source venv/bin/activate
pip3 install .

# Check against navipy
blendnavipy --background --noaudio --python-script=$PWD/navipy/scripts/check_blender_versions.py

# Make packages in the virtualenv match the one used by blender
pip3 install --upgrade  pip
pip3 install --upgrade -r requirement_blender.txt
