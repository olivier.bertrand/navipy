import sys
with open("versionblendpy.txt", "w") as f:
    f.write('.'.join([str(ii) for ii in sys.version_info[:3]]))
