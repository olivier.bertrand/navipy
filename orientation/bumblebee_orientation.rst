Bumblebee orientation during a learning flight
==============================================

Bumblebees are eusocial insects. The bees of the specie *bombus terrestris* have their hive underground, and can access it by a hole. When leaving the hive, they perform a learning and orientation flight to remember the location of the hive entrance and decide in which direction they should forage or explore the environment. Bees can be filmed while perforing their learning flight. As we have seen before, 3 points or markers need to be placed on the bee's thorax to determine its orientation. Bees have therefore been marked with three points, forming a triangle, on their thorax

.. image:: media/marked_bumblebee.png

**Note** The dataset used in this example come from recording made at the University of Bielefeld in Martin Egelhaaf's group, by Charlotte Doussot. 

Position of the thorax's markers along time
-------------------------------------------

**Note** due to occlusion during the experiment, at several time-points, the positions of the markers have not been recorded.

.. plot:: orientation/examples/bumblebee_markers.py


Yaw, pitch and roll along time
------------------------------

We first need to load the markers

.. literalinclude:: examples/bumblebee_thorax_orientation.py
   :lines: 9-10

To calculate the orientation, the euler axis convention as well as how our \
markers are placed relative to the three rotational axis of the thorax.

.. literalinclude:: examples/bumblebee_thorax_orientation.py
   :lines: 13-14

We now just have to loop frame by frame through our trajectory and \
calculate the orientation from the markers.

.. literalinclude:: examples/bumblebee_thorax_orientation.py
   :lines: 16-29

The angles are in radians. Some persons prefer to read angle in degrees

.. literalinclude:: examples/bumblebee_thorax_orientation.py
   :lines: 32

.. plot:: orientation/examples/bumblebee_thorax_orientation.py
