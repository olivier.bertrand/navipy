import pandas as pd
from matplotlib import pyplot as plt
from bodorientpy.markers.transformations import triangle2homogeous_transform
from bodorientpy.markers.triangle import Triangle
from bodorientpy.plot import draw_frame


apex0 = pd.Series(data=[0.,  0.25,  0.],
                  index=['x', 'y', 'z'])
apex1 = pd.Series(data=[0.5, -0.5,  0.],
                  index=['x', 'y', 'z'])
apex2 = pd.Series(data=[0.5,  0.5,  0.],
                  index=['x', 'y', 'z'])

mytriangle = Triangle(apex0, apex1, apex2)
frame = triangle2homogeous_transform(
    mytriangle, triangle_mode='x-axis=median-from-0')

fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111, projection='3d')
draw_frame(ax=ax, frame=frame)
mytriangle.plot(ax=ax, apex_marker='wo')
ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_zlim([-1, 1])
fig.show()
