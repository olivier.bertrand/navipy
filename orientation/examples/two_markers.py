import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from bodorientpy.homogenous.transformations import compose_matrix
import bodorientpy.markers.transformations as mtf
from bodorientpy.markers.triangle import Triangle


yaw_t = +3*np.pi/4
pitch_t = -1*np.pi/6
roll_t = -1*np.pi/12
angles = [yaw_t, pitch_t, roll_t]
euler_axes = 'zyx'
# Create a triangle in a given orientation
# and get the x,y,z axis used as our two markers
triangle_mode = 'x-axis=median-from-0'
transform = compose_matrix(angles=angles,
                           axes=euler_axes)
markers = pd.Series(data=0,
                    index=pd.MultiIndex.from_product(
                        [[0, 1, 2], ['x', 'y', 'z']]))
markers.loc[(0, 'x')] = -1
markers.loc[(2, 'y')] = np.sin(np.pi / 3)
markers.loc[(1, 'y')] = -np.sin(np.pi / 3)
markers.loc[(1, 'x')] = np.cos(np.pi / 3)
markers.loc[(2, 'x')] = np.cos(np.pi / 3)
equilateral = Triangle(markers.loc[0],
                       markers.loc[1],
                       markers.loc[2])
equilateral.transform(transform)
_, x_axis, y_axis, z_axis = mtf.triangle2bodyaxis(
    equilateral, triangle_mode)

known_angles = np.linspace(-np.pi, np.pi, 180)

axis_alignement = 'x-axis'
mark0 = pd.Series(data=0, index=['x', 'y', 'z'])
mark1 = pd.Series(x_axis, index=['x', 'y', 'z'])
solution_x_axis = pd.DataFrame(index=known_angles, columns=['yaw',
                                                            'pitch',
                                                            'roll'])
for known_angle in known_angles:
    angles_estimate = mtf.twomarkers2euler(
        mark0, mark1, axis_alignement, known_angle, euler_axes)
    solution_x_axis.loc[known_angle, :] = angles_estimate

axis_alignement = 'y-axis'
mark0 = pd.Series(data=0, index=['x', 'y', 'z'])
mark1 = pd.Series(y_axis, index=['x', 'y', 'z'])
solution_y_axis = pd.DataFrame(index=known_angles, columns=['yaw',
                                                            'pitch',
                                                            'roll'])
for known_angle in known_angles:
    angles_estimate = mtf.twomarkers2euler(
        mark0, mark1, axis_alignement, known_angle, euler_axes)
    solution_y_axis.loc[known_angle, :] = angles_estimate

axis_alignement = 'z-axis'
mark0 = pd.Series(data=0, index=['x', 'y', 'z'])
mark1 = pd.Series(z_axis, index=['x', 'y', 'z'])
solution_z_axis = pd.DataFrame(index=known_angles, columns=['yaw',
                                                            'pitch',
                                                            'roll'])
for known_angle in known_angles:
    angles_estimate = mtf.twomarkers2euler(
        mark0, mark1, axis_alignement, known_angle, euler_axes)
    solution_z_axis.loc[known_angle, :] = angles_estimate

fig, axarr = plt.subplots(1, 3, figsize=(15, 4),
                          sharey=True)
ax = axarr[0]
solution_x_axis.plot(ax=ax)
ax.set_title('x-aligned')
ax = axarr[1]
solution_y_axis.plot(ax=ax)
ax.set_title('y-aligned')
ax = axarr[2]
solution_z_axis.plot(ax=ax)
ax.set_title('z-aligned')

for ax in axarr:
    ax.set_xlabel('input angle [rad]')
    ax.set_ylabel('euler angle [rad]')

fig.show()

import time
time.sleep(5)
