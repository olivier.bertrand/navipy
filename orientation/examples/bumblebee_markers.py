import pandas as pd
import matplotlib.pyplot as plt
import pkg_resources

bumblebee_flight = pkg_resources.resource_filename(
    'bodorientpy', 'resources/bee007.hdf')
markers = pd.read_hdf(bumblebee_flight, 'markers')
markers_thorax = [0, 1, 2]

fig, axarr = plt.subplots(1, len(markers_thorax),
                          figsize=(15, 3),
                          sharey=True)
for plt_i, cmark_name in enumerate(markers_thorax):
    markers.loc[:, cmark_name].plot(ax=axarr[plt_i],
                                    linewidth=2)
    axarr[plt_i].set_title(cmark_name)
    axarr[plt_i].set_xlabel('time [frame]')
    axarr[plt_i].set_ylabel('position [mm]')
fig.show()
