import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pkg_resources
from bodorientpy.markers.transformations import markers2decompose

bumblebee_flight = pkg_resources.resource_filename(
    'bodorientpy', 'resources/bee007.hdf')
markers = pd.read_hdf(bumblebee_flight, 'markers')
markers = markers.astype(float)
markers_thorax = [0, 1, 2]

axes = 'zyx'
triangle_mode = 'y-axis=2-1'

yawpitchroll = pd.DataFrame(index=markers.index,
                            columns=['yaw', 'pitch', 'roll'],
                            dtype=float)

for frame_i, markers in markers.dropna().iterrows():
    _, _, angles, _, _ = markers2decompose(
        markers.loc[markers_thorax[0]],
        markers.loc[markers_thorax[1]],
        markers.loc[markers_thorax[2]],
        triangle_mode=triangle_mode,
        euler_axes=axes)
    yawpitchroll.yaw[frame_i] = angles[0]
    yawpitchroll.pitch[frame_i] = angles[1]
    yawpitchroll.roll[frame_i] = angles[2]


yawpitchroll_deg = np.rad2deg(yawpitchroll)

fig = plt.figure()
ax = fig.gca()
yawpitchroll_deg.plot(ax=ax)
ax.set_xlabel('time [frame]')
ax.set_ylabel('euler angle [deg]')
fig.show()
