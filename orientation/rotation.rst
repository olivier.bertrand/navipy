Rotation
========

The world is full of animated agents, e.g. animals, aircraft, and robots. They move in the world avoiding predators, foraring and transporting back suplies, or looking for the perfect (or a suitable) mate. As observer of curiosities and strangeness of the world, we wonder where such agents look at, we wonder of their orientation. In geometry the orientation ( also: angular position, or attitude ) of an object is part of the description of how it is placed in the space it is in. Namely, it is the imaginary rotation that is needed to move the object from a reference placement to its current placement. 

To introduce the different concept related to the orientation of an agent, we will first work in a imaginary world composed of only two dimensions. The position of an agent in such world can be express as a function of two variables. Your screen is indeed a two dimentional space. The position of the mouse is express in term of pixel along the height and the length of your monitor. We could for example place the mouse at the position :math:`(100,200)`. 
Wait, where should I start counting? Where is the position :math:`(0,0)`? We need to define an origin, and the direction in which we count, e.g. from left to right and from bottom to top for the first and second variable respectivly. Without knowing we are defining a reference frame. A reference frame is composed of unit vectors, i.e. the direction in which we have to count and the unit used (here the unit is the pixel), and an origin (here the bottom left corner of the monitor).

The reference frame allows to position an agent in the world, but what about its orientation. The orientation of an agent requires another frame, one link to the agent itself. We need an origin, e.g. the center of mass, and two unit vectors (because we are in a 2D space, remember). One unit vector can be chosen along the long axis of the body, and the second one orthogonal to first one. The orthogonality will ease later the formalism. We can then place the agent in its resting position, i.e. at null orientation. When the agent will move its orientation will change, i.e. the imaginary rotation that is needed to move the reference frame at the resting position to the reference attached to the agent.

A rotation is a circular movement of an object around a center (or point) of rotation. In linear algebra the rotation of an angle :math:`\alpha` is defined by the matrix:

.. math::
   R =
   \begin{bmatrix}
   \cos \alpha & -\sin \alpha \\
   \sin \alpha & \cos \alpha \\
   \end{bmatrix}

A vector :math:`v_0` can be rotated by an angle :math:`\alpha` by aplying the matrix :math:`R` to :math:`v_0`, i.e. :math:`v=Rv_0`

We will say that the agent is looking in the direction $\alpha$ when the frame link to the agent is the rotation by the angle $\alpha$ of the frame at the resting position, i.e. when

.. math::   
   F_a=RF_0

here :math:`F_a` is the actual frame of the agent, :math:`F_0` is the frame at the resting position, and :math:`R` the imaginary rotation.


Finding the orientation of an agent (2D)
----------------------------------------

Usualy we do not know the imaginary rotation made by the agent. To find it we need to invert the linar system introduced above. 

.. math::
   R=F_a(F_0)^{-1}

Once we have the orientation matrix we can find the angle :math:`\alpha` by using combination of elements of the matrix. For the present case we can get :math:`\alpha` by using the first column of the matrix.

Reference frame in the real world (3D)
--------------------------------------

The real world do not have only two dimensions but three. The reference frames will have then three unit vectors. The first unit vector can still be choosen along the longitudinal axis of the agent. But how do we define the two other one. We can no longer determined the 2nd vector by using the 1st vector and orthogonality, because the 1st vector has an infinite amount of unit vectors. We need to introduce a convention. Scientists and ingenieurs have converged to a convention for aircraft. The first vector is along the longitidunal axis, the 2nd from left to right when seated in the aircraft, and the 3rd and last one pointing downward. The last vector is used to measure height, and it makes sens for an aircraft to measure height positivly downward. 

Once the reference frame has been introduce we need to have a look at the orientation matrix. This time it will be a 3x3 matrix, i.e. composed of 9 elements. 

A rotation in a three dimentional is made around a line, i.e. an axis or a vector. We have already defined three vectors, and you know what, the orientation can be defined by three rotations. 

**Note** The frame of the agent can be computed from three none colinear points. One will be the origin, the 1st axis can go from the origin an between the two other points. The second axis is orthogonal to the plane formed by the three points. The last vector is the cross product of the two other ones. This process assumes that the distance between any two given points of a rigid body remains constant in time regardless of external forces exerted on it, i.e. the points are placed on a rigid body

**Note** The set of vectors is call in linear algebra, a basis, if the vectors are linearly independent and every vector in the vector space is a linear combination of this set. In physics, it is called a frame of reference, i.e. it consits of an abstract coordinate system and the set of physical reference points that uniquely fix (locate and orient) the coordinate system and standardize measurements.
