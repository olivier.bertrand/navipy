from navipy.trajectories.similarity.dtw.multi import multi_dtw
import pandas as pd
import numpy as np
import unittest


class TestMultiDTW(unittest.TestCase):
    def test_input_not_empty(self):
        """
        Tests if ValueError is correctly raised when at least one input is \
        empty
        """
        traj1 = pd.DataFrame()
        traj2 = pd.DataFrame(np.random.rand(5))
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_dtw(mydict)

    def test_calc_example1(self):
        """
        Tests if we get the correct value for one example calculation
        Result should be the same as the output of the module function
        fastdtw
        """
        traj1 = pd.DataFrame([[1, 1], [2, 4]])
        traj2 = pd.DataFrame([[1, 3], [3, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        res = 3.0
        # '[0]' because we get more than one return
        # and the first one is the distance
        self.assertEqual(multi_dtw(mydict)[0], res)

    def test_calc_identical(self):
        """
        Tests the function with two identical input dataframes
        Distance is then defined as 0
        """
        x = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T
        y = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T

        traj1 = pd.DataFrame(x)
        traj2 = pd.DataFrame(y)
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        res = 0

        self.assertEqual(multi_dtw(mydict)[0], res)

    def test_contain_nan1(self):
        """
        Tests that ValueError is raised when input 1 contains nans
        """
        traj1 = pd.DataFrame([[1, 1], [np.nan, 4]])
        traj2 = pd.DataFrame([[1, 3], [3, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_dtw(mydict)[0]

    def test_contain_nan2(self):
        """
        Tests that ValueError is raised when input 2 contains nans
        """
        traj1 = pd.DataFrame([[1, 1], [2, 4]])
        traj2 = pd.DataFrame([[1, 3], [np.nan, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_dtw(mydict)[0]

    def test_contain_nan_both(self):
        """
        Tests that ValueError is raised when input 2 contains nans
        """
        traj1 = pd.DataFrame([[1, 1], [np.nan, 4]])
        traj2 = pd.DataFrame([[1, 3], [np.nan, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_dtw(mydict)[0]


if __name__ == '__main__':
    unittest.main()
