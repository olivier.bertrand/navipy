from navipy.trajectories.similarity.tools import make_df
import unittest
import numpy as np


class TestMakeDF(unittest.TestCase):
    def test_test(self):
        """
        This was just a test to see if testing the tests works
        """

        self.assertTrue(True)

    def test_input_not_empty(self):
        """
        Tests if ValueError is raised correctly when giving the function\
        an empty dictionary
        """

        emptydict = {}

        with self.assertRaises(ValueError):
            make_df(emptydict)

    def test_input_not_dictionary(self):
        """
        Tests if ValueError is raised correctly when giving the function\
        not a dictionary
        """

        notadict = np.random.rand(5)

        with self.assertRaises(TypeError):
            make_df(notadict)


if __name__ == '__main__':
    unittest.main()
