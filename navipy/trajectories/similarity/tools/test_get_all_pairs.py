from navipy.trajectories.similarity.tools import get_all_pairs
import unittest
import numpy as np
import pandas as pd


class TestAllPairs(unittest.TestCase):
    def test_input_not_empty(self):
        """
        Tests if ValueError is raised correctly when giving the function\
        an empty dictionary
        """

        emptydict = {}

        with self.assertRaises(ValueError):
            get_all_pairs(emptydict)

    def test_input_not_dictionary(self):
        """
        Tests if ValueError is raised correctly when giving the function\
        not a dictionary
        """

        notadict = np.random.rand(5)

        with self.assertRaises(TypeError):
            get_all_pairs(notadict)

    def test_df_in_trajlist(self):
        """
        """
        x = pd.DataFrame(np.random.rand(5))
        y = pd.DataFrame(np.random.rand(5))
        z = np.random.rand(5)

        mylist = {'0': x, '1': y, '2': z}

        with self.assertRaises(TypeError):
            get_all_pairs(mylist)


if __name__ == '__main__':
    unittest.main()
