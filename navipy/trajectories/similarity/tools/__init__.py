"""
Tools to prepare input to similarity-functions in this package.
"""

import pandas as pd
from itertools import combinations


def dict_from_list(traj_list):
    """
    Make a dictionary from a list of dataframes with the dataframes as
    values, and numbers (0 - n-1) as keys
    :param traj_list: list of all trajectories that are later wanted to be \
    compared
    (item in list must be a dataframe)
    """

    traj_dict = {}

    if len(traj_list) <= 0:
        raise ValueError('{} is empty'.format('Trajectory-list'))

    else:
        for indx, item in enumerate(traj_list):
            traj_dict[indx] = item

        return traj_dict


def get_all_pairs(traj_dict):
    """
    Use the combination function from itertools to get all possible \
    combinations of trajectories without duplicates

    :param traj_dict: dictionary of all dataframes with numbered keys that \
    are wanted to be compared
    """
    if isinstance(traj_dict, dict) is False:
        raise TypeError(
            'Argument should be a Dictionary, not {}.'.format(type(traj_dict)))

    else:

        for item in traj_dict.values():
            if isinstance(item, pd.DataFrame) is False:
                msg = 'Item in traj-dict should be a Pandas.DataFrame, not.{}'
                raise TypeError(msg.format(type(item)))

        if len(traj_dict.values()) > 0:

            all_pairs = [ii for ii in combinations(traj_dict.items(), r=2)]

            return all_pairs

        else:
            raise ValueError('{} is empty'.format('Trajectory-dict'))


def make_df(traj_dict):
    """
    create a pandas DataFrame in which the distance of each pair will be \
    inserted

    :param traj_dict: dictionary of all dataframes with numbered keys that \
    are wanted to be compared
    """

    if isinstance(traj_dict, dict) is False:
        raise TypeError(
            'Argument should be a Dictionary, not {}.'.format(type(traj_dict)))

    else:

        if len(traj_dict.values()) > 0:
            traj_names = list(traj_dict.keys())
            index = traj_names
            columns = traj_names
            df = pd.DataFrame(index=index, columns=columns)
            df.index.name = ''

        else:
            raise ValueError('{} is empty'.format('Trajectory-dict'))

        return df
