from navipy.trajectories.similarity.tools import dict_from_list
import unittest


class TestDictList(unittest.TestCase):
    def test_input_not_empty(self):
        """
        Tests if ValueError is raised correctly when giving the \
        function an empty list
        """

        emptylist = []

        with self.assertRaises(ValueError):
            dict_from_list(emptylist)


if __name__ == '__main__':
    unittest.main()
