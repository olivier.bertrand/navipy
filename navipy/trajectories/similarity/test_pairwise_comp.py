import unittest
import pandas as pd
import numpy as np
from navipy.trajectories.similarity import pairwise_comp
from navipy.trajectories.similarity.dtw.multi import multi_dtw

class TestPairwiseComp(unittest.TestCase):
    def test_func_callable_str(self):
        """
        Test that argument 'func' is a callable object
        A string is given as a non-callable object
        """
        traj1 = pd.DataFrame(np.random.rand(3))
        traj2 = pd.DataFrame(np.random.rand(3))

        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        myfunc = 'NoFunction'

        with self.assertRaises(TypeError):
            pairwise_comp(mydict, myfunc)

    def test_func_callable_np(self):
        """
        Test that argument 'func' is a callable object
        A np array is given as a non-callable object
        """
        traj1 = pd.DataFrame(np.random.rand(3))
        traj2 = pd.DataFrame(np.random.rand(3))

        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        myfunc = np.random.rand(3)

        with self.assertRaises(TypeError):
            pairwise_comp(mydict, myfunc)

    def test_traj_list_np(self):
        """
        Tests that the argument 'traj_list'
        is either a dict or list
        Here, a np array is given
        """

        my_traj_list = np.random.rand(3)

        with self.assertRaises(TypeError):
            pairwise_comp(my_traj_list, multi_dtw)

    def test_traj_list_str(self):

        my_traj_list = 'Not a trajectory'

        with self.assertRaises(TypeError):
            pairwise_comp(my_traj_list, multi_dtw) 
        

if __name__ == '__main__':
    unittest.main()
