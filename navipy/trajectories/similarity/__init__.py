"""
Tools
~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.similarity.tools

Dynamic Time Warping
~~~~~~~
.. autofunction:: navipy.similarity.dtw.multi

Frechet-Distance
~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.similarity.frechet
.. autofunction:: navipy.similarity.frechet.multi

Time Warp Edit Distance
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.similarity.twed
.. autofunction:: navipy.similarity.twed.multi

"""


from multiprocessing import Pool
import multiprocessing
from functools import partial
from navipy.trajectories.similarity.tools import get_all_pairs
from navipy.trajectories.similarity.tools import make_df
from navipy.trajectories.similarity.tools import dict_from_list


def pairwise_comp(traj_list, func, **kwargs):
    """
    A multiprocess script based on Pool,
    map, a list of all trajectories,
    and returning the distance (based on one of three similarity measures)\
    for each pair of two trajectories, as well as the two indices \
    (row and column)

    :param traj_list: list of all trajectory-paths that are wanted to be\
    compared, similar to:
    traj_list = [[df1,df2],[df1,df3],[df1,df4],...]
    :param func: name of similarity function as a callble function) \
    'dtw', 'frechet', 'twed'
    """

    if callable(func) is False:
        msg = 'The argument "func" needs to be a callable object, not {}'
        raise TypeError(msg.format(type(func)))

    if len(kwargs) <= 0:
        myfunction = func
    else:
        myfunction = partial(func, **kwargs)

    nb_proc = multiprocessing.cpu_count() - 1
    pool = Pool(processes=nb_proc)

    if isinstance(traj_list, dict):
        traj_dict = traj_list
        results = make_df(traj_dict)  # empty df for all the distances

    elif isinstance(traj_list, list):    
        # get dictionary from list for make_df
        traj_dict = dict_from_list(traj_list)
        results = make_df(traj_dict)  # empty df for all the distances

    else:
        msg = 'The argument "traj_list" needs to be either a list \
               or dictionary, not {}'
        raise TypeError(msg.format(type(traj_list)))

    all_pairs = get_all_pairs(traj_dict)
    # We create a partial function that set the two parameters of the twed
    # so that multiprocessing will call multi_ function with these parameters.
    # calculate  distance with multiprocessing
    out = pool.imap(myfunction, all_pairs)

    for d, t1, t2 in out:

        results.loc[t1, t2] = d  # put distance in df

    return results
