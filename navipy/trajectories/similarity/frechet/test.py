import numpy as np
import unittest

from navipy.trajectories.similarity.frechet import frdist


class TestFrechet(unittest.TestCase):
    def test_test(self):
        """
        This was just a test to see if testing the tests works
        """

        self.assertTrue(True)

    def test_calc_example(self):
        """
        Tests the function against a known output with two
        different input arrays, example 1
        """
        x = [[1, 1], [2, 1], [2, 2]]
        y = [[2, 2], [0, 1], [2, 4]]
        res = 2.0

        self.assertEqual(frdist(x, y), res)

    def test_calc_identical(self):
        """
        Tests the function with two identical input arrays
        Distance is then defined as 0
        """
        x = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T
        y = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T
        res = 0

        self.assertEqual(frdist(x, y), res)

    def test_calc_example2(self):
        """
        Tests the function against a known output with two
        different input arrays, example 2
        """
        x = [[-1, 0], [0, 1], [1, 0], [0, -1]]
        y = [[-2, 0], [0, 2], [2, 0], [0, -2]]
        res = 1.0

        self.assertEqual(frdist(x, y), res)

    def test_calc_example3(self):
        """
        Tests the function against a known output with two
        different input arrays, example 3
        """
        x = np.array((np.linspace(0.0, 1.0, 100), np.ones(100)*2)).T
        y = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T
        res = 1.0

        self.assertEqual(frdist(x, y), res)

    def test_calc_example4(self):
        """
        Tests the function against a known output with two
        different input arrays, example 4
        """
        x = [[1, 4], [3, 8], [5, 4]]
        y = [[1, 1], [2, 5], [5, 3]]
        res = 3.16227766

        self.assertAlmostEqual(frdist(x, y), res)

    def test_2d_array(self):
        """
        Tests if ValueError is raised correctly if shape from x
        differs from shape of y
        """
        x = [[1, 1, 2, 1]]
        y = [[2, 2], [0, 1], [2, 4]]

        with self.assertRaises(ValueError):
            frdist(x, y)

    def test_notempty_array(self):
        """
        Tests if ValueError is raised correctly if at least one
        array is empty
        """
        x = []
        y = [[2, 2], [0, 1], [2, 4]]

        with self.assertRaises(ValueError):
            frdist(x, y)


if __name__ == '__main__':
    unittest.main()
