from navipy.trajectories.similarity.frechet import frdist
import numpy as np


def multi_frechet(one_pair):
    """
    use trajectory list to calculate the Frechet Distance
    This function will be used in the multiprocessing step \
    (see multi_pairwise_comp)
    :param one_pair: list with all trajectories \
    (map-function will loop through it, so we act like the list \
    will be one pair)
    (basically all_pairs, but the variable-name needs to be different))
    :return: dataframe with distances

    """

    traj_1 = one_pair[0][1]
    traj_2 = one_pair[1][1]

    if np.isnan(traj_1.values).any() or np.isnan(traj_2.values).any():
        raise ValueError(
            '{} and/or {} contain(s) nans'.format('Input1', 'Input2'))

    if traj_1.shape[0] > 0 and traj_2.shape[0] > 0:
        dist = frdist(traj_1, traj_2)

    else:
        msg = 'Input 1/2 (Size {}/{}) are/is empty'.format(
            traj_1.shape, traj_2.shape)
        raise ValueError(msg)

    return dist, one_pair[0][0], one_pair[1][0]
