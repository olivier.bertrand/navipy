from navipy.trajectories.similarity.twed import fast_twed
import pandas as pd
import numpy as np


def multi_twed(one_pair, elasticity=1, delpenalty=0):
    """
    use trajectory list to calculate the TWED (Time Warp Edit Distance)
    This function will be used in the multiprocessing step \
    (see multi_pairwise_comp)
    :param one_pair: list with all trajectories (map-function will loop \
    through it, so we act like the list will be one pair)
    (basically all_pairs, but the variable-name needs to be different))
    """

    traj_1 = one_pair[0][1]
    traj_2 = one_pair[1][1]

    if isinstance(traj_1, pd.DataFrame) is False:
        msg = 'traj_1 needs to be a Pandas.DataFrame and not {}'
        raise TypeError(msg.format(type(traj_1)))

    elif isinstance(traj_2, pd.DataFrame) is False:
        msg = 'traj_2 needs to be a Pandas.DataFrame and not {}'
        raise TypeError(msg.format(type(traj_2)))

    else:

        if np.isnan(traj_1.values).any() or np.isnan(traj_2.values).any():
            raise ValueError(
                '{} and/or {} contain(s) nans'.format('Input1', 'Input2'))

        if traj_1.shape[0] > 0 and traj_2.shape[0] > 0:
            dist = fast_twed(traj_1, traj_2, elasticity, delpenalty)

        else:
            msg = 'Input 1/2 (size {}/{}) are/is empty'
            raise ValueError(msg.format(traj_1.shape, traj_2.shape))

    return dist, one_pair[0][0], one_pair[1][0]
