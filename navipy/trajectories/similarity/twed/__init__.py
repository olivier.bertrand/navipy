import numpy as np
import pandas as pd
import datetime
import copy


def _cost_xy(x, y, gamma, cost_function):
    if gamma == 'x':
        # |x_p - x_{p-1}|
        return cost_function(x-np.roll(x, +1))[1:]
    elif gamma == 'y':
        # |y_p - y_{p-1}|
        return cost_function(y-np.roll(y, +1))[1:]
    elif gamma == 'xy':
        # |x_p - y_p| +  |x_{p-1} - y_{p-1}|
        mx = np.tile(x[..., np.newaxis], y.shape[0])
        my = np.tile(y[..., np.newaxis], x.shape[0]).transpose()
        return cost_function(mx-my)[1:, 1:] +\
            cost_function(np.roll(mx, 1, axis=0) -
                          np.roll(my, 1,
                                  axis=len(my.shape)-1))[1:, 1:]
    else:
        raise NameError('gamma should be either x,y or xy')


def gamma(x, y, gamma_mode, elasticity, delpenalty,
          cost_timestamp=lambda x: np.abs(x),
          cost_values=lambda x: np.sqrt(np.sum(x**2, axis=1))):
    """ Gamma function of p,q and a mode ['x','y','xy']

    $$
    \\begin{align}
    \\Gamma_{x}&=|m_{x_{p}}-m_{x_{p-1}}| + &\\gamma|t_{x_{p}}-t_{x_{p-1}}|\
    + \\lambda  \\
    \\Gamma_{xy}&=|m_{x_{p}}-m_{y_{p}}| + &\\gamma|t_{x_{p}}-t_{y_{p}}|+ \\
    |m_{x_{p-1}}-m_{y_{p-1}}| + &\\gamma|t_{x_{p-1}}-t_{y_{p-1}}|\\
    \\Gamma_{y}&=|m_{y_{p}}-m_{y_{p-1}}| + &\\gamma|t_{y_{p}}-t_{y_{p-1}}|\
    + \\lambda  \\
    \\end{align}
    $$

    :param x: a DataFrame
    :param y: a DataFrame with the same columns as x
    :param gamma: 'x','y','xy' define the gamma function to be evaluated
    """
    # Convert everything to values
    # because we are going to use function such as
    # cost_timestamp and cost_values that probably
    # expect numbers and not pandas objects
    t_x = x.index.values
    t_y = y.index.values
    m_x = x.loc[:, x.columns].values
    # Just to make sure that columns in x are also in y
    m_y = y.loc[:, x.columns].values

    time_cost = _cost_xy(t_x, t_y, gamma_mode, cost_timestamp)
    values_cost = _cost_xy(m_x, m_y, gamma_mode, cost_values)

    if gamma_mode == 'x':
        toreturn = pd.Series(index=np.arange(1, t_x.shape[0]),
                             data=values_cost+elasticity*time_cost+delpenalty)
        toreturn.index.name = 'p'
    if gamma_mode == 'y':
        toreturn = pd.Series(index=np.arange(1, t_y.shape[0]),
                             data=values_cost+elasticity*time_cost+delpenalty)
        toreturn.index.name = 'q'
    if gamma_mode == 'xy':
        toreturn = pd.DataFrame(
            index=np.arange(1, t_x.shape[0]),
            columns=np.arange(1, t_y.shape[0]),
            data=values_cost+elasticity*time_cost)
        toreturn = toreturn.unstack()
        toreturn.index.names = ['q', 'p']
    return toreturn


def _twed(p, q, _gammas_function):
    """Recursively called function"""
    if (p <= 0) & (q <= 0):
        delta = 0
    elif (p <= 0) | (q <= 0):
        delta = np.inf
    else:
        delete_x = _twed(p-1, q, _gammas_function) + \
            _gammas_function['x'].loc[p]
        g = _gammas_function['xy'].loc[(q, p)]
        match = _twed(p-1, q-1, _gammas_function) + g
        g = _gammas_function['y'].loc[q]
        delete_y = _twed(p, q-1, _gammas_function) + g
        delta = np.min([delete_x, match, delete_y])
    return delta


def slow_twed(traj_x, traj_y, elasticity=1, delpenalty=0):
    """ Time Warp Edit Distance

    :param traj_x: a pandas DataFrame
    :param traj_y: a pandas DataFrame with same columns as traj_x
    :param elasticity: time penalty $\\gamma$ in equation
    :param delpenalty: deletion penalty $\\lambda$ in equation
    """
    _gammas_function = dict()
    for mode in ['x', 'y', 'xy']:
        _gammas_function[mode] = gamma(
            traj_x, traj_y, mode, elasticity=elasticity, delpenalty=delpenalty)
    p, q = traj_x.shape[0]-1, traj_y.shape[0]-1
    toreturn = _twed(p, q, _gammas_function)
    return toreturn


def __memoize(func):
    mem = dict()

    def memoizer(**kwargs):
        key = str(kwargs)
        if key not in mem:
            mem[key] = func(**kwargs)
        return mem[key]
    return memoizer


@__memoize
def _fast_twed(p, q, cdate_mem):
    """Recursively called function

    Memorize serves to avoid recomputing the function, by
    storing return value of function called.
    """
    global _gammas_function
    if (p == 0) & (q == 0):
        delta = 0
    elif (p == 0) | (q == 0):
        delta = np.inf
    else:
        delete_x = _fast_twed(p=p-1, q=q, cdate_mem=cdate_mem) + \
            _gammas_function['x'].loc[p]
        g = _gammas_function['xy'].loc[(q, p)]
        match = _fast_twed(p=p-1, q=q-1, cdate_mem=cdate_mem) + g
        g = _gammas_function['y'].loc[q]
        delete_y = _fast_twed(p=p, q=q-1, cdate_mem=cdate_mem) + g
        delta = np.min([delete_x, match, delete_y])
    return delta


def memleak_fast_twed(traj_x, traj_y, elasticity=1, delpenalty=0):
    """ Time Warp Edit Distance

    The fast version memorize result from certain evaluation
    for p, and q in order to not repeat the already calculated
    evalutation. It saves a lot of computation time

    see https://www.python-course.eu/levenshtein_distance.php
    for more detail about the idea

    :param traj_x: a pandas DataFrame
    :param traj_y: a pandas DataFrame with same columns as traj_x
    :param elasticity: time penalty $\\gamma$ in equation
    :param delpenalty: deletion penalty $\\lambda$ in equation
    """
    if (traj_x.shape[0] <= 0) or (traj_y.shape[0] <= 0):
        return np.nan
    if '_gammas_function' in globals():
        _gfunc_copy = copy.copy(globals()['_gammas_function'])
        recopy = True
    else:
        recopy = False
        global _gammas_function

    _gammas_function = dict()
    for mode in ['x', 'y', 'xy']:
        _gammas_function[mode] = gamma(
            traj_x, traj_y, mode, elasticity=elasticity, delpenalty=delpenalty)
    p, q = traj_x.shape[0]-1, traj_y.shape[0]-1
    cdate_mem = datetime.datetime.now().strftime('%Y%m%d_%H%M%S.%f')
    # Init memoriser
    global mem
    mem = {}
    toreturn = _fast_twed(p=p, q=q, cdate_mem=cdate_mem)
    mem = None
    if recopy:
        _gammas_function = _gfunc_copy
    else:
        del globals()['_gammas_function']

    return toreturn


class FastTWED():
    def __init__(self, traj_x, traj_y):
        self._traj_x = traj_x.copy()
        self._traj_y = traj_y.copy()
        # Init cost function
        self._cost_timestamp = lambda x: np.abs(x)
        self._cost_values = lambda x: np.sqrt(np.sum(x**2, axis=1))
        # Init elasticity and delpenaltity
        self.elasticity = 0
        self.delpenalty = 0
        # Init the gamma functions
        self._gammas_function = dict()

    def gamma_function(self):
        self._gammas_function = dict()
        for mode in ['x', 'y', 'xy']:
            self._gammas_function[mode] = gamma(
                self._traj_x, self._traj_y,
                mode,
                elasticity=self.elasticity,
                delpenalty=self.delpenalty,
                cost_timestamp=self._cost_timestamp,
                cost_values=self._cost_values)
        return self._gammas_function

    def fast_twed(self):
        if (self._traj_x.shape[0] <= 1) or (self._traj_y.shape[0] <= 1):
            return -np.inf
        self._gammas_function = self.gamma_function()
        self._memorize = dict()
        p, q = self._traj_x.shape[0]-1, self._traj_y.shape[0]-1
        delta = self._fast_twed(p, q)
        self._memorize = None
        return delta

    def _fast_twed(self, p, q):
        """Recursively called function

        Memorize serves to avoid recomputing the function, by
        storing return value of function called.
        """
        if (p <= 0) & (q <= 0):
            delta = 0
        elif (p <= 0) | (q <= 0):
            delta = np.inf
        else:
            i1 = (p-1, q)
            g1 = self._gammas_function['x'].loc[p]
            if i1 not in self._memorize:
                # delete_x
                self._memorize[i1] = self._fast_twed(*i1)

            i2 = (p, q-1)
            g2 = self._gammas_function['y'].loc[q]
            if i2 not in self._memorize:
                # delete_y
                self._memorize[i2] = self._fast_twed(*i2)

            i3 = (p-1, q-1)
            g3 = self._gammas_function['xy'].loc[(q, p)]
            if i3 not in self._memorize:
                # match
                self._memorize[i3] = self._fast_twed(*i3)

            delta = min([self._memorize[i1]+g1,
                         self._memorize[i2]+g2,
                         self._memorize[i3]+g3])
        return delta


def fast_twed(traj_x, traj_y, elasticity=1, delpenalty=0):

    if isinstance(traj_x, pd.DataFrame) is False:
        msg = 'traj_x needs to be a Pandas.DataFrame and not {}'
        raise TypeError(msg.format(type(traj_x)))

    elif isinstance(traj_y, pd.DataFrame) is False:
        msg = 'traj_y needs to be a Pandas.DataFrame and not {}'
        raise TypeError(msg.format(type(traj_y)))

    elif delpenalty < 0:
        msg = 'delpenalty should be greater or equal than zero and not{}'
        raise ValueError(msg.format(delpenalty))

    elif elasticity <= 0:
        msg = 'elasticity should be strictly greater than zero and not{}'
        raise ValueError(msg.format(elasticity))

    else:

        mytwed = FastTWED(traj_x, traj_y)
        mytwed.elasticity = elasticity
        mytwed.delpenalty = delpenalty
        return mytwed.fast_twed()
