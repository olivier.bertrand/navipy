from navipy.trajectories.similarity.twed.multi import multi_twed
import pandas as pd
import numpy as np
import unittest


class TestMultiTWED(unittest.TestCase):

    def test_input_not_empty(self):
        """
        Tests if ValueError is correctly raised when at least one input is \
        empty
        """
        traj1 = pd.DataFrame()
        traj2 = pd.DataFrame(np.random.rand(5))
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_twed(mydict)

    def test_calc_example1(self):
        """
        Tests if we get the correct value for one example calculation
        Result should be the same as the output of the module function
        fast_twed
        """

        x = [3, 6, 8]
        y = [1, 8, 7]

        traj1 = pd.DataFrame(x, index=[1, 3, 8])
        traj2 = pd.DataFrame(y, index=[2, 5, 9])

        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        res = 13.0
        # '[0]' because we get more than one return
        # and the first one is the distance
        self.assertEqual(multi_twed(mydict)[0], res)

    def test_calc_identical(self):
        """
        Tests the function with two identical input dataframes
        Distance is then defined as 0
        """
        x = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T
        y = np.array((np.linspace(0.0, 1.0, 100), np.ones(100))).T

        traj1 = pd.DataFrame(x)
        traj2 = pd.DataFrame(y)
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        res = 0

        self.assertEqual(multi_twed(mydict)[0], res)

    def test_return_not_none(self):  # not necessary?
        """
        Tests that the output of the function is not none
        Not really necessary though, better would be raising an \
        error in the function if it is empty, and then checking \
        if error is raised correctly (?)
        """
        x = pd.DataFrame(np.random.rand(3))
        y = pd.DataFrame(np.random.rand(3))

        mydict = {'0': x, '1': y}
        mydict = list(mydict.items())

        self.assertIsNotNone(multi_twed(mydict)[0])

    def test_input1_df(self):
        """
        Tests that a TypeError is raised correctly when the first
        argument to the fast_twed function is not a DataFrame, within
        the multifast_twed function
        """

        x = np.random.rand(3)
        y = pd.DataFrame(np.random.rand(3))

        mydict = {'0': x, '1': y}
        mydict = list(mydict.items())

        with self.assertRaises(TypeError):
            multi_twed(mydict)

    def test_input2_df(self):
        """
        Tests that a TypeError is raised correctly when the second
        argument to the fast_twed function is not a DataFrame, within
        the multifast_twed function
        """

        x = pd.DataFrame(np.random.rand(3))
        y = np.random.rand(3)

        mydict = {'0': x, '1': y}
        mydict = list(mydict.items())

        with self.assertRaises(TypeError):
            multi_twed(mydict)

    def test_greater_zero_del(self):
        """
        Tests that a ValueError is raised correctly when the
        value for the elasticity parameter is smaller than or equal
        to 0. The parameter is only defined for > 0.
        """
        x = pd.DataFrame(np.random.rand(3))
        y = pd.DataFrame(np.random.rand(3))

        mydict = {'0': x, '1': y}
        mydict = list(mydict.items())

        delpenalty = -1

        with self.assertRaises(ValueError):
            multi_twed(mydict, delpenalty=delpenalty)

    def test_greater_zero_elast(self):
        """
        Tests that a ValueError is raised correctly when the
        value for the elasticity parameter is smaller than or equal
        to 0. The parameter is only defined for > 0.
        """
        x = pd.DataFrame(np.random.rand(3))
        y = pd.DataFrame(np.random.rand(3))

        mydict = {'0': x, '1': y}
        mydict = list(mydict.items())

        elasticity = 0

        with self.assertRaises(ValueError):
            multi_twed(mydict, elasticity=elasticity)

    def test_contain_nan1(self):
        """
        Tests that ValueError is raised when input 1 contains nans
        """
        traj1 = pd.DataFrame([[1, 1], [np.nan, 4]])
        traj2 = pd.DataFrame([[1, 3], [3, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_twed(mydict)

    def test_contain_nan2(self):
        """
        Tests that ValueError is raised when input 2 contains nans
        """
        traj1 = pd.DataFrame([[1, 1], [2, 4]])
        traj2 = pd.DataFrame([[1, 3], [np.nan, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_twed(mydict)

    def test_contain_nan_both(self):
        """
        Tests that ValueError is raised when input 2 contains nans
        """
        traj1 = pd.DataFrame([[1, 1], [np.nan, 4]])
        traj2 = pd.DataFrame([[1, 3], [np.nan, 4]])
        mydict = {'0': traj1, '1': traj2}
        mydict = list(mydict.items())

        with self.assertRaises(ValueError):
            multi_twed(mydict)


if __name__ == '__main__':
    unittest.main()
