import unittest
import pandas as pd
import numpy as np
from navipy.trajectories.similarity.twed import fast_twed


class TestFastTwed(unittest.TestCase):
    def test_calc_example(self):
        """
        Tests the function against a known output with two
        different input arrays, example 1
        """
        x = [3, 6, 8]
        x = pd.DataFrame(x, index=[1, 3, 8])
        y = [1, 8, 7]
        y = pd.DataFrame(y, index=[2, 5, 9])
        res = 13
        self.assertEqual(fast_twed(x, y), res)

    def test_calc_identical(self):
        """
        Tests the function with two identical input arrays
        Distance is then defined as 0
        """
        x = [3, 6, 8]
        x = pd.DataFrame(x, index=[1, 3, 8])
        y = [3, 6, 8]
        y = pd.DataFrame(x, index=[1, 3, 8])
        res = 0
        self.assertEqual(fast_twed(x, y), res)

    def test_return_not_none(self):  # not necessary?
        """
        Tests that the output of the function is not none
        Not really necessary though, better would be raising an \
        error in the function if it is empty, and then checking \
        if error is raised correctly
        """
        x = pd.DataFrame(np.random.rand(3))
        y = pd.DataFrame(np.random.rand(3))
        self.assertIsNotNone(fast_twed(x, y))

    def test_input1_df(self):
        """
        Tests that a TypeError is raised correctly when the first
        argument to the fast_twed function is not a DataFrame
        """

        x = np.random.rand(3)
        y = pd.DataFrame(np.random.rand(3))

        with self.assertRaises(TypeError):
            fast_twed(x, y)

    def test_input2_df(self):
        """
        Tests that a TypeError is raised correctly when the second
        argument to the fast_twed function is not a DataFrame
        """

        x = pd.DataFrame(np.random.rand(3))
        y = np.random.rand(3)

        with self.assertRaises(TypeError):
            fast_twed(x, y)

    def test_greater_zero_del(self):
        """
        Tests that a ValueError is raised correctly when the
        value for the elasticity parameter is smaller than or equal
        to 0. The parameter is only defined for > 0.
        """
        x = pd.DataFrame(np.random.rand(3))
        y = pd.DataFrame(np.random.rand(3))
        delpenalty = -1

        with self.assertRaises(ValueError):
            fast_twed(x, y, delpenalty=delpenalty)

    def test_greater_zero_elast(self):
        """
        Tests that a ValueError is raised correctly when the
        value for the elasticity parameter is smaller than or equal
        to 0. The parameter is only defined for > 0.
        """
        x = pd.DataFrame(np.random.rand(3))
        y = pd.DataFrame(np.random.rand(3))
        elasticity = 0

        with self.assertRaises(ValueError):
            fast_twed(x, y, elasticity=elasticity)


if __name__ == '__main__':
    unittest.main()
