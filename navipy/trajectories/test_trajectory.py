import unittest
import numpy as np
import pandas as pd
from navipy.maths import constants as htconst
from navipy.trajectories import Trajectory
from navipy.trajectories import _markerstransform, _invmarkerstransform


class TestTrajectoryTransform(unittest.TestCase):
    def setUp(self):
        # Create a random trajectory
        trajectory = Trajectory(indeces=np.arange(10),
                                rotconv='xyz')
        trajectory.x = 100 * (np.random.rand(trajectory.shape[0]) - 0.5)
        trajectory.y = 100 * (np.random.rand(trajectory.shape[0]) - 0.5)
        trajectory.z = 100 * (np.random.rand(trajectory.shape[0]) - 0.5)
        trajectory.alpha_0 = 2 * np.pi * \
            (np.random.rand(trajectory.shape[0]) - 0.5)
        trajectory.alpha_1 = 2 * np.pi * \
            (np.random.rand(trajectory.shape[0]) - 0.5)
        trajectory.alpha_2 = 2 * np.pi * \
            (np.random.rand(trajectory.shape[0]) - 0.5)
        self.trajectory = trajectory

    def test_forwardreverse(self):
        # Build an equilateral triangle
        markers = pd.Series(data=0,
                            index=pd.MultiIndex.from_product([
                                [0, 1, 2],
                                ['x', 'y', 'z']]))
        markers.loc[(0, 'x')] = -1
        markers.loc[(2, 'y')] = np.sin(np.pi / 3)
        markers.loc[(1, 'y')] = -np.sin(np.pi / 3)
        markers.loc[(1, 'x')] = np.cos(np.pi / 3)
        markers.loc[(2, 'x')] = np.cos(np.pi / 3)
        markers.index.name = None
        trajectory = self.trajectory.copy()
        # Create test trajectory
        traj_test = Trajectory(indeces=np.arange(10),
                               rotconv='xyz')
        # Devide by two the second anle, because of gimbal lock
        col = (trajectory.rotation_mode, 'alpha_1')
        trajectory.loc[:, col] = trajectory.loc[:, col] / 2
        # forward
        for euler_axes in list(htconst._AXES2TUPLE.keys()):
            trajectory.rotation_mode = euler_axes
            traj_test.rotation_mode = euler_axes
            transformed_markers = trajectory.body2world(markers)
            # reverse
            for triangle_mode in ['x-axis=median-from-0',
                                  'y-axis=1-2']:
                traj_test.from_markers(transformed_markers,
                                       triangle_mode)
                np.testing.assert_array_almost_equal(
                    trajectory.astype(float),
                    traj_test.astype(float),
                    err_msg='Trajectory and traj from markers')

            # Test world2body
            # We compare for two time points
            # world2body will give world marker along traj in body coordinate
            for idx in [trajectory.index[0], trajectory.index[-1]]:
                markers_test = trajectory.world2body(
                    transformed_markers.loc[idx, :])
                markers_test = markers_test.loc[idx, :]
                np.testing.assert_array_almost_equal(
                    markers_test,
                    markers,
                    err_msg='body2world->world2body')

    def test_raise_world2body(self):
        trajectory = self.trajectory.copy()
        with self.assertRaises(TypeError):
            # Not a series
            trajectory.body2world(markers=pd.DataFrame)
        with self.assertRaises(TypeError):
            # No multindex
            trajectory.body2world(markers=pd.Series(index=[0]))

    def test_markerstransform(self):
        trajectory = self.trajectory.copy()
        homogeneous_markers = pd.DataFrame(index=[0, 1],
                                           columns=['x', 'y', 'z', 'w'],
                                           data=np.random.rand(2, 4))
        homogeneous_markers.loc[:, 'w'] = 1
        # Make sure that columns are correctly ordered
        homogeneous_markers = homogeneous_markers[['x', 'y', 'z', 'w']]
        # Transpose because we apply homogeneous transformation
        # on the marker, and thus a 4x4 matrix on a 4xN matrix
        # here N is the number of markers
        homogeneous_markers = homogeneous_markers.transpose()
        for idx in [trajectory.index[0], trajectory.index[-1]]:
            marker = _markerstransform(idx, trajectory,
                                       homogeneous_markers,
                                       rotation_mode=None)
            marker = marker.unstack()
            marker['w'] = 1
            # Make sure that columns are correctly ordered
            marker = marker[['x', 'y', 'z', 'w']]
            # Transpose because we apply homogeneous transformation
            # on the marker, and thus a 4x4 matrix on a 4xN matrix
            # here N is the number of markers
            marker = marker.transpose()

            # Apply reverse
            imarker = _invmarkerstransform(idx, trajectory,
                                           marker,
                                           rotation_mode=None)
            imarker = imarker.unstack()
            imarker['w'] = 1
            # Make sure that columns are correctly ordered
            imarker = imarker[['x', 'y', 'z', 'w']]
            # Transpose because we apply homogeneous transformation
            # on the marker, and thus a 4x4 matrix on a 4xN matrix
            # here N is the number of markers
            imarker = imarker.transpose()

            # Compare
            np.testing.assert_array_almost_equal(imarker, homogeneous_markers)

    def test_velocity(self):
        pass

    def test_traveldist(self):
        indeces = np.linspace(0, 2*np.pi, 2000)
        radius = 5
        mytraj = Trajectory(indeces=indeces, rotconv='zyx')
        mytraj.x = radius*np.cos(indeces)
        mytraj.y = radius*np.sin(indeces)
        mytraj.z = 0
        # The length of function above
        # is equal to perimeter of the circle
        # 2*pi*r
        travel_dist_theo = 2*np.pi*radius
        travel_dist = mytraj.traveled_distance()
        np.testing.assert_almost_equal(
            travel_dist, travel_dist_theo, decimal=4)
        # Test with nans
        mytraj.iloc[[15, 50, 90], :] = np.nan
        travel_dist = mytraj.traveled_distance()
        np.testing.assert_almost_equal(
            travel_dist, travel_dist_theo, decimal=4)

    def test_sinuosity(self):
        indeces = np.linspace(0, np.pi, 1000)
        radius = 5
        mytraj = Trajectory(indeces=indeces, rotconv='zyx')
        mytraj.x = radius*np.cos(indeces)
        mytraj.y = radius*np.sin(indeces)
        mytraj.z = 0
        # The length  of function above
        # is equal to half the perimeter of the circle
        # pi*r
        # the sinuosity will be equal to:
        sinuosity_theo = np.pi*radius/(2*radius)
        sinuosity = mytraj.sinuosity()
        np.testing.assert_almost_equal(sinuosity, sinuosity_theo, decimal=4)


if __name__ == '__main__':
    unittest.main()
