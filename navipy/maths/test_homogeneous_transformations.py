import unittest
import numpy as np
from navipy.maths import homogeneous_transformations as ht
from navipy.maths import random
from navipy.maths import euler
from navipy.maths import quaternion as quat


class TestHT(unittest.TestCase):
    def test_translation_matrix(self):
        vector = np.random.random(3)
        vector -= 0.5
        vector_test = ht.translation_matrix(vector)[:3, 3]
        np.testing.assert_allclose(vector,
                                   vector_test)

    def test_translation_from_matrix(self):
        vector = np.random.random(3)
        vector -= 0.5
        matrix = ht.translation_matrix(vector)
        vector_test = ht.translation_from_matrix(matrix)
        np.testing.assert_allclose(vector,
                                   vector_test)

    def test_decompose_matrix_translation(self):
        translation_0 = ht.translation_matrix([1, 2, 3])
        angles, trans = ht.decompose_matrix(translation_0)
        translation_1 = ht.translation_matrix(trans)
        np.testing.assert_allclose(translation_0, translation_1)

    def test_decompose_matrix_rotation(self):
        rotation_0 = euler.matrix(1, 2, 3, 'xyz')
        angles, _ = ht.decompose_matrix(rotation_0)
        rotation_1 = euler.matrix(*angles, 'xyz')
        np.testing.assert_allclose(rotation_0, rotation_1)

    def test_is_same_transform(self):
        matrix = np.random.rand(4, 4)
        self.assertTrue(ht.is_same_transform(matrix, matrix))

    def test_is_same_transform_rejection(self):
        # The determinant of a rotation matrix is one,
        # Thus we test rejection rotation against scaled identity
        matrix = 2 * np.identity(4)
        rotation = random.rotation_matrix()
        self.assertFalse(ht.is_same_transform(matrix, rotation))

    def test_compose_decompose(self):
        angles_o = [0.123, -1.234, 2.345]
        translation_o = [1, 2, 3]
        axes = 'xyz'
        matrix = ht.compose_matrix(angles_o,
                                   translation_o,
                                   axes=axes)
        angles, trans = ht.decompose_matrix(matrix, axes)
        np.testing.assert_almost_equal(angles, angles_o)
        np.testing.assert_almost_equal(trans, translation_o)

        matrix_1 = ht.compose_matrix(angles,
                                     trans, axes)
        self.assertTrue(ht.is_same_transform(matrix, matrix_1))
        # Test quaternion
        quaternion_o = quat.from_euler(*angles_o, axes)
        axes = 'quaternion'
        matrix = ht.compose_matrix(quaternion_o,
                                   translation_o, axes=axes)
        quaternion, trans = ht.decompose_matrix(
            matrix, axes)
        np.testing.assert_almost_equal(quaternion, quaternion_o)
        np.testing.assert_almost_equal(trans, translation_o)

    def test_44nonull(self):
        with self.assertRaises(ValueError):
            ht.decompose_matrix(np.zeros((4, 4)))


if __name__ == '__main__':
    unittest.main()
