"""
"""
import numpy as np
from navipy.maths import constants
from navipy.maths import euler
from navipy.maths import quaternion


def translation_matrix(direction):
    """
    Return matrix to translate by direction vector
    """
    # get 4x4 identity matrix
    tmatrix = np.identity(4)
    # set the first three rows of the last colum to be the direction vector
    tmatrix[:3, 3] = direction[:3]
    return tmatrix


def translation_from_matrix(matrix):
    """
    Return translation vector from translation matrix
    """
    # returns the first three rows of the last colum in the rotation matrix
    return np.array(matrix, copy=False)[:3, 3].copy()


def decompose_matrix(matrix, axes='xyz'):
    """Return sequence oftransformations from transformation matrix.

    matrix : array_like
            Non-degenerative homogeneous transformation matrix compose
            of rotation and translation

    Return tuple of:
            angles : list of Euler angles about static x, y, z axes
            translate : translation vector along x, y, z axes

    Raise ValueError if matrix is of wrong type or degenerative.
    """
    M = np.array(matrix, dtype=np.float64, copy=True)
    if abs(M[3, 3]) < constants._EPS:
        raise ValueError("M[3, 3] is zero")
    M /= M[3, 3]
    translate = M[:3, 3].copy()
    rotate = M[:3, :3].copy()
    if axes == 'quaternion':
        angles = quaternion.from_matrix(rotate)
    else:
        angles = euler.from_matrix(rotate, axes)
    return angles, translate


def compose_matrix(angles=None, translate=None, axes='xyz'):
    """Return transformation matrix from sequence oftransformations.

    This is the inverse of the decompose_matrix function.

    Sequence of transformations:
            angles : list of Euler angles about static x, y, z axes
            translate : translation vector along x, y, z axes
    """
    M = np.identity(4)
    if translate is not None:
        T = np.identity(4)
        T[:3, 3] = translate[:3]
        M = np.dot(M, T)
    if angles is not None:
        if axes == 'quaternion':
            R = quaternion.matrix(angles)
        else:
            R = euler.matrix(angles[0], angles[1], angles[2], axes)
        M = np.dot(M, R)
    M /= M[3, 3]
    return M


def is_same_transform(matrix0, matrix1):
    """Return True if two matrices perform same transformation.
    """
    matrix0 = np.array(matrix0, dtype=np.float64, copy=True)
    matrix0 /= matrix0[3, 3]
    matrix1 = np.array(matrix1, dtype=np.float64, copy=True)
    matrix1 /= matrix1[3, 3]
    return np.allclose(matrix0, matrix1)


def testing_is_same_transform(matrix0, matrix1):
    """Return True if two matrices perform same transformation.
    """
    matrix0 = np.array(matrix0, dtype=np.float64, copy=True)
    matrix0 /= matrix0[3, 3]
    matrix1 = np.array(matrix1, dtype=np.float64, copy=True)
    matrix1 /= matrix1[3, 3]
    np.testing.assert_allclose(matrix0, matrix1)
