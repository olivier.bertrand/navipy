import unittest
import numpy as np
from navipy.maths import homogeneous_transformations as ht
from navipy.maths import quaternion as quat
from navipy.maths import random as random


class TestQuaternions(unittest.TestCase):
    def test_quaternion_from_euler(self):
        quaternion = quat.from_euler(1, 2, 3, 'yxz')
        self.assertTrue(np.allclose(quaternion,
                                    [0.435953, 0.310622,
                                     -0.718287, 0.444435]))

    def test_about_axis(self):
        quaternion = quat.about_axis(0.123, [1, 0, 0])
        self.assertTrue(np.allclose(quaternion,
                                    [0.99810947, 0.06146124, 0, 0]))

    def test_matrix(self):
        quaternion = np.random.rand(4)
        quaternion /= np.sqrt(np.sum(quaternion**2))
        matrix = quat.matrix(quaternion)
        quaternion_fm = quat.from_matrix(matrix)
        np.testing.assert_allclose(quaternion_fm, quaternion)

    def test_matrix_identity(self):
        matrix = quat.matrix([1, 0, 0, 0])
        self.assertTrue(np.allclose(matrix, np.identity(4)))

    def test_matrix_diagonal(self):
        matrix = quat.matrix([0, 1, 0, 0])
        self.assertTrue(np.allclose(matrix, np.diag([1, -1, -1, 1])))

    def test_from_matrix_identity(self):
        quaternion = quat.from_matrix(np.identity(4))
        self.assertTrue(np.allclose(quaternion, [1, 0, 0, 0]))

    def test_from_matrix_diagonal(self):
        quaternion = quat.from_matrix(np.diag([1, -1, -1, 1]))
        self.assertTrue(np.allclose(quaternion, [0, 1, 0, 0])
                        or np.allclose(quaternion, [0, -1, 0, 0]))

    def test_from_matrix_rotation(self):
        rotation = random.rotation_matrix()
        quaternion = quat.from_matrix(rotation)
        ht.testing_is_same_transform(
            rotation, quat.matrix(quaternion))

    def test_from_matrix_eq141(self):
        """Test equation 141 of Diebel 2006"""
        r = np.identity(4)
        self.assertEqual(quat.from_matrix_case(r), 'eq141')
        quaternion = quat.from_matrix(r)
        r_test = quat.matrix(quaternion)
        np.testing.assert_array_almost_equal(r, r_test)

    def test_from_matrix_eq142(self):
        """Test equation 142 of Diebel 2006"""
        r = np.array([[0.29192658,  0.45464871, -0.84147098,  0.],
                      [-0.83722241, -0.30389665, -0.45464871,  0.],
                      [-0.46242567,  0.83722241,  0.29192658,  0.],
                      [0.,  0.,  0.,  1.]])
        self.assertEqual(quat.from_matrix_case(r), 'eq142')
        quaternion = quat.from_matrix(r)
        r_test = quat.matrix(quaternion)
        np.testing.assert_array_almost_equal(r, r_test)

    def test_from_matrix_eq143(self):
        """Test equation 143 of Diebel 2006"""
        r = np.array([[-0.59957301,  0.08214901, -0.7960928, 0.],
                      [-0.28933129,  0.90519074,  0.31131515,  0.],
                      [0.74619006, 0.41699072, -0.51895966,  0.],
                      [0.,          0.,          0.,          1.]])
        self.assertEqual(quat.from_matrix_case(r), 'eq143')
        quaternion = quat.from_matrix(r)
        r_test = quat.matrix(quaternion)
        np.testing.assert_array_almost_equal(r, r_test)

    def test_from_matrix_eq144(self):
        """Test equation 144 of Diebel 2006"""
        r = np.array([[0.25189738,  0.80456467,  0.53779495,  0.],
                      [-0.62809165, -0.28685348,  0.72333668,  0.],
                      [0.73623949, -0.51999114,  0.43308271,  0.],
                      [0.,          0.,          0.,          1.]])
        self.assertEqual(quat.from_matrix_case(r), 'eq144')
        quaternion = quat.from_matrix(r)
        r_test = quat.matrix(quaternion)
        np.testing.assert_array_almost_equal(r, r_test)

    def test_inverse(self):
        quaternion_0 = random.quaternion()
        quaternion_1 = quat.inverse(quaternion_0)
        self.assertTrue(np.allclose(
            quat.multiply(quaternion_0, quaternion_1),
            [1, 0, 0, 0]))

    def test_mytest(self):
        val = quat.conjugate([2, 3.0, 4])
        self.assertTrue((val == [2, -3, -4]).all())

    def test_real(self):
        val1 = quat.real([2, 3.0, 4])
        val2 = quat.real([5.0, 4, 7])
        self.assertEqual(val1, 2.0)
        self.assertEqual(val2, 5.0)

    def test_imag(self):
        val = quat.imag([2, 4.0, 6, 3.0, 5.0, 10, 9.0])
        self.assertTrue((val == [4, 6, 3]).all())

    def test_diff(self):
        axis, angle = quat.diff([2, 3.0, 4, 5], [4, 8.0, 2, 3])
        self.assertTrue(np.allclose(
            axis, [0.09230769,  0.29230769, -0.61538462]))
        self.assertEqual(angle, 1.7370787905717791)
        """for testing length 0 condition"""
        a, b = quat.diff([2, 0, 0, 0], [3, 0, 0, 0])
        self.assertTrue(np.allclose(a, [1, 0, 0]))


if __name__ == '__main__':
    unittest.main()
