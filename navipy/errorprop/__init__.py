"""
Numerical error propagation

tools to propogate the error by using an estimate
of the jacobian matrix (matrix of derivatives)
"""
import numpy as np
import scipy.signal as signal


def estimate_jacobian(fun, x, args=None, epsilon=1e-6):
    """Estimate the jacobian matrix

    :param fun: The objective function to be derivated. Must be in \
the form f(x, *args). \
The argument, x, is a 1-D array of points, and args is a tuple of \
any additional \
fixed parameters needed to completely specify the function.
    :param x: values at which the jacobian should be calculated
    :param args: Extra arguments passed to the objective function
    :param epsilon: step used to estimate the jacobian
    :returns: An estimated jacobian matrix
    """
    if isinstance(x, (list, tuple, np.ndarray)):
        jacobian_matrix = list()
        for vari, _ in enumerate(x):
            x_minus = x.copy()
            x_plus = x.copy()
            x_minus[vari] -= epsilon
            x_plus[vari] += epsilon

            if args is None:
                dfs1 = fun(x_minus)
                dfs2 = fun(x_plus)
            else:
                dfs1 = fun(x_minus, args)
                dfs2 = fun(x_plus, args)
            dfs1 = np.array(dfs1)
            dfs2 = np.array(dfs2)
            deriv = (dfs2 - dfs1) / (2 * epsilon)
            jacobian_matrix.append(deriv)
        return np.array(jacobian_matrix).transpose()
    else:
        x_minus = x - epsilon
        x_plus = x + epsilon
        if args is None:
            deriv = (fun(x_plus) - fun(x_minus)) / (2 * epsilon)
        else:
            deriv = (fun(x_plus, args) - fun(x_minus, args)) / (2 * epsilon)
        return deriv


def propagate_error(fun, x, covar, args=None, epsilon=1e-6):
    """Estimate the jacobian matrix

    :param fun: The objective function to be error propagated. \
Must be in the form f(x, *args). \
The argument, x, is a 1-D array of points, and args is a tuple of \
any additional \
fixed parameters needed to completely specify the function.
    :param x: values at which the error should be calculated
    :param covar: variance-covariance matrix
    :param args: Extra arguments passed to the objective function
    :param epsilon: step used to estimate the jacobian
    :returns: An estimated jacobian matrix
    """
    jacobian_matrix = estimate_jacobian(fun, x, args, epsilon=1e-6)
    if isinstance(x, (list, tuple, np.ndarray)):
        return jacobian_matrix.dot(covar.dot(jacobian_matrix.transpose()))
    else:
        return np.abs(jacobian_matrix) * covar


def _ys(k, i):
    ys = np.zeros(k)
    ys[i] = 1
    return ys


def cspline1d_errorprop(sig, lamb):
    """Calc the coefficient for cspline1d_errorpopagation

    According to equation 10b in ref 1 and cspline1d which is mirror symetric
    Thus the values of the bj differ only at the border

    1. Enting IG, Trudinger CM, Etheridge DM. Propagating data uncertainty \
    through smoothing spline fits.
    Tellus, Ser B Chem Phys Meteorol. 2006;58(4):305–9. \
    Doi:10.1111/j.1600-0889.2006.00193.x
    """
    k = sig.shape[0]
    if k <= 0:
        return np.zeros_like(sig)
    bj = np.inf+np.zeros(k)
    i = 0
    bj[i] = signal.cspline1d(_ys(k, i), lamb)[i]
    # Forward
    while(not np.isclose(bj[i], bj[i-1])):
        i += 1
        if i >= k:
            return bj
        bj[i] = signal.cspline1d(_ys(k, i), lamb)[i]
    # The constant part
    bj[i:(bj.shape[0]-i)] = bj[i]
    # End
    for i in np.arange(bj.shape[0]-i, bj.shape[0]):
        bj[i] = signal.cspline1d(_ys(k, i), lamb)[i]
    return bj


def cspline1d_errorprop_eval(bj, signal_var, newx, dx=1.0, x0=0):
    """Propagate error through cspline1d

    1. Enting IG, Trudinger CM, Etheridge DM. Propagating data uncertainty \
    through smoothing spline fits.
    Tellus, Ser B Chem Phys Meteorol. 2006;58(4):305–9. \
    Doi:10.1111/j.1600-0889.2006.00193.x
    """
    newx = (np.asarray(newx) - x0) / float(dx)
    res = np.zeros_like(newx)
    if res.size == 0:
        return res
    N = len(bj)
    cond1 = newx < 0
    cond2 = newx > (N - 1)
    cond3 = ~(cond1 | cond2)
    # handle general mirror-symmetry
    res[cond1] = cspline1d_errorprop_eval(bj, signal_var, -newx[cond1])
    res[cond2] = cspline1d_errorprop_eval(
        bj, signal_var, 2 * (N - 1) - newx[cond2])
    newx = newx[cond3]
    if newx.size == 0:
        return res
    result = np.zeros_like(newx)
    jlower = np.floor(newx - 2).astype(int) + 1
    for i in range(4):
        thisj = jlower + i
        indj = thisj.clip(0, N - 1)  # handle edge cases
        # See equation 13 of ref [1]
        result += signal_var[indj]*((bj[indj] * signal.cubic(newx - thisj))**2)
    res[cond3] = result
    return res
