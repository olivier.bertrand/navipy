"""
Blender come with its own python version and uses
common packages.
Some of these packages are also used by navipy.
To avoid having issues, it is best to build navipy
with the same version of packages as blender

The script return the requirements for navipy+blender install
"""
from platform import python_version
import os

# This list should be equivalent to there
# the 'requires' entry of the
# setup_dict in setup.py
packages = ['numpy',
            'pandas',
            'matplotlib',
            'scipy',
            'networkx',
            'ipython',
            'PIL',
            'yaml',
            'cv2',
            'fastdtw',
            'statsmodels',
            'sklearn']
blacklisted_package = ['PIL',
                       'yaml',
                       'cv2',
                       'sklearn']
filereq = 'requirement_blender.txt'
# Look for packages require by blender and navipy
# there versions should match
requirements = []
print('Look for packages')
for pkg in packages:
    try:
        cmod = __import__(pkg)
    except ImportError as e:
        # Not use by blender so no incompatibilty issues
        continue
    try:
        line = pkg + '==' + cmod.__version__
    except AttributeError as e:
        # Package as no __version__
        continue
    print('\t', line)
    if pkg in blacklisted_package:
        print(pkg, ' version is not checked')
        continue
    requirements.append(line)
# Write a requirement file to auto install the packages
# prior to navipy with the correct versions
print('Write Requirement file ', end='')
with open(filereq, 'w') as cfile:
    for line in requirements:
        cfile.write(line + '\n')
print('Ok')

# Display user informations
pythonvec = python_version()
pathreq = os.path.abspath(filereq)
msg = '#---With anaconda---\n'
msg += '#You can create an anaconda virtual environment as follow\n'
msg += 'conda update conda\n'
msg += 'conda create -n myblendnavipy python={} anaconda\n'.format(
    pythonvec)
msg += 'activate myblendnavipy\n'
msg += 'conda install --yes --file {}\n'.format(pathreq)
msg += 'conda install navipy\n'
print(msg)

msg = '#---With mkvirtualenv/pip---\n'
msg += '#You can create an virtual environment as follow\n'
msg += 'pip3 install virtualenv\n'
msg += 'virtualenv --python=python{} myblendnavipy\n'.format(pythonvec)
msg += 'source myblendnavipy/bin/activate\n'
msg += 'pip3 install --upgrade pip\n'
msg += 'pip3 install -r {}\n'.format(pathreq)
msg += 'pip3 install navipy\n'
print(msg)
