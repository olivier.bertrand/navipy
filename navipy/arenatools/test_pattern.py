import unittest
import numpy as np
import navipy.arenatools.patterns as tobetested
# import matplotlib.pyplot as plt


class TestCase(unittest.TestCase):
    def setUp(self):
        self.width = 201
        self.height = 100
        self.beta = 2
        self.pixel_per_mm = 2
        self.image = tobetested.rectangular_pattern(self.width,
                                                    self.height,
                                                    self.beta,
                                                    self.pixel_per_mm)

    def test_rectangular_patter(self):
        """Chech the image dimension"""
        # Check image dimension
        # * height
        # * width
        self.assertEqual(self.image.shape[0],
                         self.height * self.pixel_per_mm)
        self.assertEqual(self.image.shape[1],
                         self.width * self.pixel_per_mm)

    def test_norm_img(self):
        """Check that normalised image span [0,255]"""
        im = np.random.rand(100, 101)
        im = tobetested.norm_img(im)
        self.assertEqual(im.min(), 0)
        self.assertEqual(im.max(), 255)

    def test_gray2red(self):
        """Check that green=blue, red == max"""
        red = tobetested.gray2red(self.image.copy())
        # The red channel should be equal to max
        np.testing.assert_array_equal(red[..., 0], self.image.max())
        # Green==Blue
        np.testing.assert_array_equal(red[..., 1], red[..., 2])


if __name__ == '__main__':
    unittest.main()
