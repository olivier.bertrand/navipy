#! /bin/bash
log_file='./rebuild_db_log.log'
# Path are relative to the current folder
# because we want to rebuild resources
# that are used in unit testing and the documentation

#-----
# Rebuild first database
#-----
db_filename='./database.db'
blender_world='./twocylinders_world.blend'
blender_config='./configs/BlenderOnGridRender.yaml'
# move file just in case
mv $db_filename $db_filename'.old'
# run command
blendongrid --blender-world $blender_world --config-file $blender_config --output-file $db_filename --logfile $log_file -vvv

#----
# Rebuild second database
#----
db_filename='./database2.db'
blender_world='./forest_world.blend'
blender_config='./configs/BlenderRender.yaml'
traj_file='./gridforest.csv'
# move file just in case
mv $db_filename $db_filename'.old'
# run command
blendalongtraj --blender-world $blender_world --config-file $blender_config --output-file $db_filename --trajectory $traj_file --logfile $log_file -vvv
