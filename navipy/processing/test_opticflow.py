import unittest
import navipy.processing.mcode as mcode
import pandas as pd
import numpy as np
from navipy.scene import __spherical_indeces__
from navipy.trajectories import posorient_columns
from navipy.trajectories import velocities_columns


def Scale(data, oldmin, oldmax, mini, maxi, ran):
    """Scales all values in data into the range mini, maxi
    Input:
        - data: values to be scaled
        - mini: minimum of new range
        - maxi: maximum of new range
        - ran: range of new values (ran=maxi-mini)

    Output:
        - data: the scaled data"""
    data = (data-oldmin)/oldmax
    dmax = np.max(data)
    dmin = np.min(data)
    scaling = ran/(dmax-dmin)
    data = (data-dmin)*scaling
    data = data+mini
    return data


class TestCase(unittest.TestCase):
    def setUp(self):
        """sets up a distance array that is used in some tests,
           as well as the velocity and viewing direction frames
        """
        distance = 2.6 + np.zeros((5, 5))

        self.scene = np.zeros((5, 5, 4, 1))
        self.scene[:, :, 0, 0] = np.array(distance).copy()
        self.scene[:, :, 1, 0] = np.array(distance).copy()
        self.scene[:, :, 2, 0] = np.array(distance).copy()
        self.scene[:, :, 3, 0] = np.array(distance).copy()
        convention = 'xyz'
        self.convention = convention
        tuples_posvel = posorient_columns(convention)
        tuples_posvel.extend(velocities_columns(convention))
        index_posvel = pd.MultiIndex.from_tuples(tuples_posvel,
                                                 names=['position',
                                                        'orientation'])
        velocity = pd.Series(index=index_posvel, data=0)
        # Most test are independent of orientation and position
        velocity.loc[(convention, 'alpha_0')] = 2 * \
            np.pi * (np.random.rand() - 0.5)
        velocity.loc[(convention, 'alpha_1')] = np.pi * \
            (np.random.rand() - 0.5)
        velocity.loc[(convention, 'alpha_2')] = 2 * \
            np.pi * (np.random.rand() - 0.5)
        velocity.loc[('location', 'x')] = np.random.randn()
        velocity.loc[('location', 'y')] = np.random.randn()
        velocity.loc[('location', 'z')] = np.random.randn()
        self.velocity = velocity
        self.convention = convention

        # Init the viewing directions
        elevation = np.linspace(-np.pi / 2, np.pi / 2, 5)
        azimuth = np.linspace(-np.pi, np.pi, 5)
        [ma, me] = np.meshgrid(azimuth, elevation)
        imshape = me.shape
        viewing_directions = np.zeros((me.shape[0], ma.shape[0], 2))
        viewing_directions[..., __spherical_indeces__['elevation']] = me
        viewing_directions[..., __spherical_indeces__['azimuth']] = ma
        self.viewing_directions = viewing_directions
        self.elevation = elevation
        self.azimuth = azimuth

        # Init a random scene
        scene = np.random.rand(imshape[0], imshape[1])
        self.scene = scene

    def test_distance(self):
        """
        The magnitude of the optic flow when the agent is moving \
close to object is larger than when it is moving (with the same velocity)\
far from them.
        Here we test this property.
        """
        vel = self.velocity.copy()
        # This is true for any translational motion
        vel.loc[('location', 'dx')] = np.random.randn()
        vel.loc[('location', 'dy')] = np.random.randn()
        vel.loc[('location', 'dz')] = np.random.randn()
        #
        rof, hof, vof =\
            mcode.optic_flow(self.scene,
                             self.viewing_directions,
                             vel)
        hnorm = np.sqrt(hof**2 + vof ** 2)
        # Add abs tol because we compare to zero
        np.testing.assert_allclose(rof, np.zeros_like(rof), atol=1e-7)
        # Calculate second optic flow, with objects further away form agent
        rof_further, hof_further, vof_further =\
            mcode.optic_flow(self.scene + 1,
                             self.viewing_directions,
                             vel)
        hnorm_further = np.sqrt(hof_further**2 + vof_further**2)
        # Add abs tol because we compare to zero
        np.testing.assert_allclose(rof_further,
                                   np.zeros_like(rof_further), atol=1e-7)
        # The translational optic flow norm should be small
        # i.e. for norm equal to zero
        valid = (hnorm != 0) & (hnorm_further != 0)
        np.testing.assert_array_less(hnorm_further[valid], hnorm[valid])

    def test_wrong_convention(self):
        """ this test checks if an error is raised
        when none or a non supported convention is used
        """
        # Check for no convention
        velocity = pd.Series(index=['x', 'y', 'z',
                                    'alpha_0', 'alpha_1', 'alpha_2',
                                    'dx', 'dy', 'dz',
                                    'dalpha_0', 'dalpha_1', 'dalpha_2'],
                             data=0)
        with self.assertRaises(Exception):
            rof, hof, vof = mcode.optic_flow(self.scene,
                                             self.viewing_directions,
                                             velocity)
        # Check for wrong convention
        for convention in ['alsjf', '233']:
            velocity = self.velocity.copy()
            # Change convention with a one that will raise an error
            velocity.index.set_levels(velocity.index.levels[0].str.replace(self.convention,
                                                                           convention),
                                      level=0, inplace=True)
            with self.assertRaises(ValueError):
                rof, hof, vof = mcode.optic_flow(self.scene,
                                                 self.viewing_directions,
                                                 velocity)

    def test_rotation_not_too_strong(self):
        """
        this test checks that a Value Error is throught,
        if the change in rotation is more than pi/2.
        Thoughs, if the velocity of yaw, pitch and roll is
        is smaller than pi/2
        """
        posorients = dict()
        posorients['dyaw_toobig'] = np.array([[-20, -20, 2.6, 1.57079633, 0, 0],
                                              [-19.8, -20, 2.6,
                                               1.57079633 + np.pi/2 + 0.00001, 0, 0]])
        posorients['dpitch_toobig'] = np.array([[-20, -20, 2.6, 1.57079633, 0, 0],
                                                [-19.8, -20, 2.6,
                                                 1.57079633, 0 + np.pi/2 + 0.0001, 0]])
        posorients['droll_toobig'] = np.array([[-20, -20, 2.6, 1.57079633, 0, 0],
                                               [-19.8, -20, 2.6, 1.57079633, 0,
                                                0 + np.pi/2 + 0.0001]])

        for key, posorient in posorients.items():
            dposvel = np.diff(posorient, axis=0)
            # We swap to easily access x,y,z,etc.
            velocity = self.velocity.copy().swaplevel()
            velocity.loc[['x', 'y', 'z']] = posorient[0, :3]
            velocity.loc[['alpha_0', 'alpha_1', 'alpha_2']] = posorient[0, 3:]
            velocity.loc[['dx', 'dy', 'dz']] = dposvel[0, :3]
            velocity.loc[['dalpha_0', 'dalpha_1', 'dalpha_2']] = dposvel[0, 3:]
            # We restore correct order
            velocity = velocity.swaplevel()

            with self.assertRaises(ValueError):
                rof, hof, vof = mcode.optic_flow(self.scene,
                                                 self.viewing_directions,
                                                 velocity)

    def test_only_x(self):
        """
        this test checks for the correct response if for example
        the bee moves only in x-direction keeping the other
        parameters (y,z,yaw,pitch,roll) constant
        """
        posorient = np.array([[-20, -20, 2.6, 0, 0, 0],
                              [-19.8, -20, 2.6, 0, 0, 0]])
        dposvel = np.diff(posorient, axis=0)
        # We swap to easily access x,y,z,etc.
        velocity = self.velocity.copy().swaplevel()
        velocity.loc[['x', 'y', 'z']] = posorient[0, :3]
        velocity.loc[['alpha_0', 'alpha_1', 'alpha_2']] = posorient[0, 3:]
        velocity.loc[['dx', 'dy', 'dz']] = dposvel[0, :3]
        velocity.loc[['dalpha_0', 'dalpha_1', 'dalpha_2']] = dposvel[0, 3:]
        # We restore correct order
        velocity = velocity.swaplevel()

        rof, hof, vof = mcode.optic_flow(self.scene,
                                         self.viewing_directions,
                                         velocity)
        # rof is always 0
        testrof = np.zeros(self.viewing_directions.shape[:2])
        np.testing.assert_array_almost_equal(rof, testrof)

        # Max hof is along + 90 azimuth
        idxmax = np.unravel_index(hof.argmax(),
                                  self.viewing_directions.shape[:2])
        viewmax = np.rad2deg(self.viewing_directions[idxmax[0], idxmax[1], :])
        self.assertAlmostEqual(viewmax[__spherical_indeces__['azimuth']],
                               90)
        # Min hof is along - 90 azimuth
        idxmin = np.unravel_index(hof.argmin(),
                                  self.viewing_directions.shape[:2])
        viewmin = np.rad2deg(self.viewing_directions[idxmin[0], idxmin[1], :])
        self.assertAlmostEqual((viewmin[__spherical_indeces__['azimuth']]),
                               -90)

        # Max vof is along 0/180 azimuth with elevation !=0
        idxmax = np.unravel_index(vof.argmax(),
                                  self.viewing_directions.shape[:2])
        viewmax = np.rad2deg(self.viewing_directions[idxmax[0], idxmax[1], :])
        rest = np.mod(viewmax[__spherical_indeces__['azimuth']], 180)
        self.assertAlmostEqual(rest, 0)

        # Norm of of is null at the focus of expension or contraction
        nof = hof**2+vof**2
        idxzero = np.unravel_index(np.abs(nof).argmin(),
                                   self.viewing_directions.shape[:2])
        viewzero = np.rad2deg(
            self.viewing_directions[idxzero[0], idxzero[1], :])
        rest = np.mod(viewzero, 180)
        np.testing.assert_array_almost_equal(rest, rest*0)

    def test_only_yaw(self):
        """
        this test checks for the correct response if for example
        the bee rotates only around the yaw axis keeping the other
        parameters (x,y,z,pitch,roll) constant
        """
        # yaw only everything zero
        # only vertical should change, horizontal stays same
        posorient = np.array([[-20, -20, 2.6, 0, 0, 1.570796330],
                              [-20, -20, 2.6, 0, 0, 1.900796330]])

        dposvel = np.diff(posorient, axis=0)
        # We swap to easily access x,y,z,etc.
        velocity = self.velocity.copy().swaplevel()
        velocity.loc[['x', 'y', 'z']] = posorient[0, :3]
        velocity.loc[['alpha_0', 'alpha_1', 'alpha_2']] = posorient[0, 3:]
        velocity.loc[['dx', 'dy', 'dz']] = dposvel[0, :3]
        velocity.loc[['dalpha_0', 'dalpha_1', 'dalpha_2']] = dposvel[0, 3:]
        # We restore correct order
        velocity = velocity.swaplevel()

        rof, hof, vof = mcode.optic_flow(self.scene,
                                         self.viewing_directions,
                                         velocity)

        # rof is always 0
        testrof = np.zeros(self.viewing_directions.shape[:2])
        np.testing.assert_array_almost_equal(rof, testrof)
        # nof is zero at the pol
        nof = np.sqrt(hof**2+vof**2)
        idxzero = np.unravel_index(np.abs(nof).argmin(),
                                   self.viewing_directions.shape[:2])
        viewzero = np.rad2deg(
            self.viewing_directions[idxzero[0], idxzero[1], :])
        rest = np.mod(viewzero[__spherical_indeces__['elevation']], 90)
        self.assertAlmostEqual(rest, 0)

        # Velocity is equal to max optic flow
        self.assertAlmostEqual(velocity.loc[(self.convention, 'dalpha_2')],
                               nof.max())

        # here the azimuthal optic flow should be zero and the
        # elevational optic flow should be proportional with a
        # constant factor to the cos of the elevation
        cosel = np.cos(self.viewing_directions[...,
                                               __spherical_indeces__['elevation']])
        not_zeros = np.where(np.abs(cosel) > 1e-7)
        factor = np.array(nof[not_zeros]/np.abs(cosel[not_zeros]))
        factor = factor.flatten()
        np.testing.assert_almost_equal(factor, factor[0])

    def test_only_pitch(self):
        """
        this test checks for the correct response if for example
        the bee rotates only around the pitch axis keeping the other
        parameters (x,y,z,yaw,roll) constant
        """
        # pitch only everything zero
        # only vertical should change, horizontal stays same
        posorient = np.array([[-20, -20, 2.6, 0, 1.570796330, 0],
                              [-20, -20, 2.6, 0, 1.900796330, 0]])

        dposvel = np.diff(posorient, axis=0)
        # We swap to easily access x,y,z,etc.
        velocity = self.velocity.copy().swaplevel()
        velocity.loc[['x', 'y', 'z']] = posorient[0, :3]
        velocity.loc[['alpha_0', 'alpha_1', 'alpha_2']] = posorient[0, 3:]
        velocity.loc[['dx', 'dy', 'dz']] = dposvel[0, :3]
        velocity.loc[['dalpha_0', 'dalpha_1', 'dalpha_2']] = dposvel[0, 3:]
        # We restore correct order
        velocity = velocity.swaplevel()

        rof, hof, vof = mcode.optic_flow(self.scene,
                                         self.viewing_directions,
                                         velocity)

        # rof is always 0
        testrof = np.zeros(self.viewing_directions.shape[:2])
        np.testing.assert_array_almost_equal(rof, testrof)
        # nof is zero at along y-axis
        nof = np.sqrt(hof**2+vof**2)
        idxzero = np.unravel_index(np.abs(nof).argmin(),
                                   self.viewing_directions.shape[:2])
        viewzero = np.rad2deg(
            self.viewing_directions[idxzero[0], idxzero[1], :])
        rest = np.mod(viewzero[__spherical_indeces__['azimuth']], 90)
        self.assertAlmostEqual(rest, 0)

        # Velocity is equal to max optic flow
        self.assertAlmostEqual(velocity.loc[(self.convention, 'dalpha_1')],
                               nof.max())

    def test_only_roll(self):
        """
        this test checks for the correct response if for example
        the bee rotates only around the pitch axis keeping the other
        parameters (x,y,z,yaw,roll) constant
        """

        # yaw only everything zero
        # only vertical should change, horizontal stays same
        posorient = np.array([[-20, -20, 2.6, 1.570796330, 0, 0],
                              [-20, -20, 2.6, 1.900796330, 0, 0]])

        dposvel = np.diff(posorient, axis=0)
        # We swap to easily access x,y,z,etc.
        velocity = self.velocity.copy().swaplevel()
        velocity.loc[['x', 'y', 'z']] = posorient[0, :3]
        velocity.loc[['alpha_0', 'alpha_1', 'alpha_2']] = posorient[0, 3:]
        velocity.loc[['dx', 'dy', 'dz']] = dposvel[0, :3]
        velocity.loc[['dalpha_0', 'dalpha_1', 'dalpha_2']] = dposvel[0, 3:]
        # We restore correct order
        velocity = velocity.swaplevel()

        rof, hof, vof = mcode.optic_flow(self.scene,
                                         self.viewing_directions,
                                         velocity)

        # rof is always 0
        testrof = np.zeros(self.viewing_directions.shape[:2])
        np.testing.assert_array_almost_equal(rof, testrof)
        # nof is zero at along x-axis
        nof = np.sqrt(hof**2+vof**2)
        idxzero = np.unravel_index(np.abs(nof).argmin(),
                                   self.viewing_directions.shape[:2])
        viewzero = np.rad2deg(
            self.viewing_directions[idxzero[0], idxzero[1], :])
        rest = np.mod(viewzero[__spherical_indeces__['azimuth']], 180)
        self.assertAlmostEqual(rest, 0)

        # Velocity is equal to max optic flow
        self.assertAlmostEqual(velocity.loc[(self.convention, 'dalpha_0')],
                               nof.max())


if __name__ == '__main__':
    unittest.main()
