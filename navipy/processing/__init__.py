"""
Skyline
~~~~~~~
.. autofunction:: navipy.processing.pcode.skyline

Michelson-contrast
~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.processing.pcode.michelson_contrast

Contrast-weighted-nearness
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.processing.pcode.contrast_weighted_nearness

Place-code vectors
~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.processing.pcode.pcv

Average place-code vector
~~~~~~~~~~~~~~~~~~~~~~~~~
.. autofunction:: navipy.processing.pcode.apcv

"""
