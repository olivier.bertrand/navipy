We are glad that you are considering contributing to navipy, and are
looking forward to your contribution.

# Pre-requisit
We strongly recommand you to download, install, test, and compile the documentation

## Installation
```
git clone git@gitlab.ub.uni-bielefeld.de:olivier.bertrand/navipy.git
cd navipy
python setup.py install
```

## Unittesting
From the root directory of the repository

```
python -m unittest discover navipy
```

## Blend-unittesting
Several functionalty of navipy requires Blender, and thus code has to be run within blender (see doc for details). Unittesting of function requiring blender can be done as follow

```
blendunittest
```

Note: this command require blender to be installed

## Doc building
The doc is contained in doc/source

```
cd doc
make html
```

# Convention
We follow the PEP8 recommendation and the code should be formatted as such.

# Testing
Ideally every function should be properly tested. Test file should match the pattern 'test*.py' here the * should match the module to be tested. For example to test function within maths.py, a file named test_maths.py should be created and stored in the same folder than maths.py

Certain functionalities require the use of blender. Such test need to be stored in a file 'blendtest*.py'. Note that the blendunittest command uses the blender environment navipy/resources/twocylinders_world.blend. Therefore your test function should use this environment as well.

# Documentation
The documentation is based on Sphinx syntax. Although documenting functions is a must, tutorial is a bonus that will strongly encourage.
