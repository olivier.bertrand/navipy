rem Install navipy within blender
rem How to use it:
rem Open the command line tool with administrator rights,
rem find where python used by blender is placed, and then,
rem run this command with the path to python.exe
@echo off
echo %1 
%1 -m ensurepip
%1 -m pip install navipy
