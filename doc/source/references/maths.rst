Maths
=====

.. automodule:: navipy.maths
   :members:
      
Constants
---------

.. automodule:: navipy.maths.constants
   :members:
      
Coordinates
-----------

.. automodule:: navipy.maths.coordinates
   :members:
      
Homogeneous transformations
---------------------------

.. automodule:: navipy.maths.homogeneous_transformations
   :members:
      
Euler
~~~~~

.. automodule:: navipy.maths.euler
   :members:
      
Quaternions
~~~~~~~~~~~

.. automodule:: navipy.maths.quaternion
   :members:
      
Random generator
----------------

.. automodule:: navipy.maths.random
   :members:
      
Tools
-----

.. automodule:: navipy.maths.tools
   :members:
