Getting started
===============

Installing navipy
-----------------
It is recommanded to install navipy in a \
`virtual environment <https://virtualenvwrapper.readthedocs.io/en/latest/install.html>`_ . By doing \
so, the python version installed on the system and the one used by \
navipy may differ, and ease the use of blender rendering engine. 

.. code-block:: bash

   python setup.py install

Testing navipy
--------------

.. code-block:: bash

   python -m unittest discover navipy

Testing blender
---------------

Navipy comes with a command line tool to run your script under blender. \
It is however required that the python version used by blender, and the one use by navipy matches. To test your installation of blender and navipy you can download the (:download:`script <../../navipy/sensors/blendnavipy_test.py>`) and run it under blender with the following command:

 .. code-block:: bash
		 
   blendnavipy --python-script='blendnavipy_test.py' 

If you plan using the BlenderRenderer of navipy you may want to test that the code is passing. Download the source code, and next to setup.py run the comand:

.. dode-block:: bash

   blendunittest
   

The core modules
----------------

* Rendering
* Processing
* Comparing
* Moving
