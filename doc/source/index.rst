.. Navigation_Toolbox documentation master file, created by
   sphinx-quickstart on Mon Aug 28 11:58:29 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

About navipy
============
Navipy is a package providing method to simulate a navigating agent in realistic environment. Navipy make use of the blender rendering engine and its python API. Therefore any environment that can be realised with blender, can be used by navipy. Blender rendering may however be too slow to test different model of navigation or the different parameters of a given model. To speed up the simulation, the scene at relevant location in the environment can be stored in a database. Then, instead of rendering the scene again, it is simply loaded from the database. The agent can, thus, jump from relevant location to the next (i.e. move on a grid) without the need to use the blender API. Moreover, certain models of navigation do not depend on the history of the agent, but instead associate a moving direction to the current agent location. With such model, a graph can be created from the moving direction at each location on a grid of relevant places. The graph make use of the package networkx, thus attracting points or loop can be found, the possible connection from one point on the grid to another can be assessed, the number of independent sub graph can be calculated, etc.

Goals
=====
The navigation toolbox aims to bring in an intuitive python toolbox different methods to model the visual navigation of a virtual agent. 

* Keep the interface simple
* Allow complex environments and realistic world scenario
* Implement the most popular model of insect navigation
* Avoid re-rendering by using grid constrained motion.

Content
=======

.. toctree::
   :maxdepth: 1
	      
   gettingstarted
   tutorials/index
   references/index
   tests/index
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
