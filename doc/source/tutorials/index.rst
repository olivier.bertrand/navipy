Tutorials
=========

.. toctree::
   :maxdepth: 1

   01-building-arena.ipynb
   02-CreatingPatterns.ipynb
   02-recording-animal-trajectory.ipynb
   02a-orientation-background.ipynb
   02b-orientation-3markers.ipynb
   02c-orientation-2markers.ipynb
   03-rendering-along-trajectory.ipynb
   04-image-processing.ipynb
   04-comparing.ipynb
   04-optic-flow.ipynb
   04-error-propagation.ipynb
   05-classification.ipynb
      
Average place-code vector homing
--------------------------------
Homing with an average skyline vector consist of deriving the skyline \
or an approximation of it from the visual information. For example, \
ultra violet light is mostly present in the sky, and thus by summing \
ultra violet light along the elevation one may obtain the skyline. \
This approximation was inspired by the visual system of insect and has \
been succesffuly applied to model of homing (Basten and mallott), and robots (Thomas Stone). \

Once the skyline have been optained, the center of mass of it is calcualted. \
The center of mass of the skyline is a vector lying in the equatorial \
plane of the visual system (due to the sumation along the elevation). \
The center of mass of the skyline was succeffully applied in simulation \
and robots (Hafner, Mangan).

The center of mass of the skyline, also refered as average skyline \
vector, at the goal and current location are compared by simple difference. \
The difference gives the homing vector, i.e. a vector proportional to \
the velocity of the agent.

Our agent needs to have a function to convert its current state to a motion. \
This function, velocity, can be added as follow:

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 12-30

On a grid
~~~~~~~~~

By restricting the agent motion on a grid, we can used a database containing \
images rendered at pre defined location (the grid nodes).

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 36

And initialise the senses of our virtual agent

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 37

Now we have to initialise an agent moving on a grid (i.e. a GridAgent)

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 39

at an initial position

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 42-44

a mode of motion corresponding to the grid used in the database

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 47-51

Finally our agent is ready to fly for a number of step or until its velocity is null.

.. literalinclude:: examples/asv_homing_grid.py
   :lines: 54-55
