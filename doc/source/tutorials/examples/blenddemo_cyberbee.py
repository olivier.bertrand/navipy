import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from navipy.sensors.renderer import BlenderRender

# Configure the rendering module
cyberbee = BlenderRender()
cyberbee.cycle_samples = 100
cyberbee.camera_rotation_mode = 'XYZ'
cyberbee.camera_fov = [[-90, 90], [-180, 180]]
cyberbee.gaussian_width = 1.5
cyberbee.camera_resolution = [360, 180]

# Set the position at which to render an image/distance
posorient = pd.Series(index=['x', 'y', 'z',
                             'alpha_0', 'alpha_1', 'alpha_2'])
posorient.x = 0
posorient.y = 0
posorient.z = 1.8
posorient.alpha_0 = np.pi / 2
posorient.alpha_1 = 0
posorient.alpha_2 = 0
scene = cyberbee.scene(posorient)

# plot
f, axarr = plt.subplots(2, 1,  sharex=True)

to_plot_im = scene[:, :, :3, 0]
to_plot_dist = 1 / scene[:, :, 3, 0]

ax = axarr[0]
ax.imshow(to_plot_im)
ax.invert_yaxis()
ax.set_aspect('equal')
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_ylabel('Elevation')
ax.set_title('Panoramic view')

ax = axarr[1]
im = ax.imshow(to_plot_dist)
ax.invert_yaxis()
ax.set_aspect('equal')
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_xlabel('Azimuth')
ax.set_ylabel('Elevation')
ax.set_title('Nearness map')

f.subplots_adjust(right=0.8)
cbar_ax = f.add_axes([0.85, 0.15, 0.03, 0.3])
cb = f.colorbar(im, cax=cbar_ax)
cb.set_label('Nearness')

f.show()
f.savefig('renderimage.svg')
