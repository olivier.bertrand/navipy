import pandas as pd
import numpy as np
import pkg_resources
from navipy.database import DataBaseLoad
from navipy.processing.pcode import apcv
from navipy.moving.agent import GraphAgent
from navipy import Brain


# 0) Define a class heriting from Brain
class ASVBrain(Brain):
    def __init__(self, renderer=None):
        Brain.__init__(self, renderer=renderer)
        # Init memory
        posorient = mydb.posorients.loc[12, :]
        mybrain.update(posorient)
        self.memory = apcv(mybrain.vision.scene,
                           mybrain.vision.viewing_directions)

    def velocity(self):
        asv = apcv(self.vision.scene,
                   self.vision.viewing_directions)
        homing_vector = self.memory - asv
        homing_vector = np.squeeze(homing_vector[..., 0, :])
        velocity = pd.Series(data=0,
                             index=['dx', 'dy', 'dz',
                                    'dalpha_0', 'dalpha_1', 'dalpha_2'])
        velocity[['dx', 'dy', 'dz']] = homing_vector
        return velocity


# 1) Connect to the database
mydb_filename = pkg_resources.resource_filename(
    'navipy', 'resources/database.db')
mydb = DataBaseLoad(mydb_filename)
mybrain = ASVBrain(renderer=mydb)
# Create a grid agent
my_agent = GraphAgent(mybrain)

# Mode of motion
mode_of_motion = dict()
mode_of_motion['mode'] = 'on_cubic_grid'
mode_of_motion['param'] = dict()
mode_of_motion['param']['grid_spacing'] = 1
my_agent.mode_of_motion = mode_of_motion

# Run
my_agent.build_graph(ncpu=2, timeout=100)
print(my_agent.catchment_area())
