"""
Example on how to use the rendering module
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch
from navipy.database import DataBaseLoad
from navipy.sensors.bee_sampling import BeeSampling
from navipy.sensors.renderer import BlenderRender


# create a bee sampling
cyberbee = BlenderRender()
cyberbee.cycle_samples = 100  # higher value -> slower but nicer
bee_samp = BeeSampling(cyberbee)
# Create a list of point from which we want to render images
# Careful three d meshing grow quickly.
x = np.linspace(-5, 5, 3)
y = np.linspace(-5, 5, 3)
z = np.linspace(1.8, 1.8, 1)
alpha_1 = np.array([0]) + np.pi / 2
alpha_2 = np.array([0])
alpha_3 = np.array([0])
bee_samp.create_sampling_grid(
    x, y, z, alpha1=alpha_1, alpha2=alpha_2, alpha3=alpha_3)
# Assign maximum world dimension. Otherwise the distance
# will go to infinity, and the distance to objects after compression
# will be set to 0
world_dim = 50 * np.sqrt(2)
bee_samp.world_dim = world_dim
# Rendering in a temporary folder.
filename_db = 'database.db'
bee_samp.render(filename_db)

# --- End of database generation.
# --- Plot some point of the db
# Load and plot db
mydb = DataBaseLoad(filename_db)

# Plot a view of db
posorients = mydb.posorients
posorient = posorients.loc[1, :]
f, axarr = plt.subplots(3, 3, figsize=(15, 10))
ax = axarr[1][1]
ax.plot(posorients.x, posorients.y, 'ko')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_aspect('equal')
ax.text(0, 2.5, 'Top view of the grid',
        horizontalalignment='center', fontsize=15)
unique_x = posorients.x.unique()[::-1]
unique_y = posorients.y.unique()
for i, axar in enumerate(axarr):
    for j, ax in enumerate(axar):
        if j == 1 and i == 1:
            continue
        posorient.x = unique_x[j]
        posorient.y = unique_y[i]
        scene = mydb.scene(posorient)
        to_plot_im = scene[:, :, :3, 0]
        ax.imshow(to_plot_im)
        ax.invert_yaxis()
        ax.set_aspect('equal')
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax.set_title('Panorama at ({},{})'.format(posorient.x, posorient.y))
        xyA = [0, 0]
        if j == 0:
            xyA[0] = to_plot_im.shape[1]
        elif j == 1:
            xyA[0] = to_plot_im.shape[1] // 3
        elif j == 2:
            xyA[0] = 0
        print(i, j, posorient.x, posorient.y)
        if i == 0:
            xyA[1] = 0
        elif i == 1:
            xyA[1] = to_plot_im.shape[0] // 3
        elif i == 2:
            xyA[1] = to_plot_im.shape[0]

        con = ConnectionPatch(xyA=xyA,
                              xyB=(posorient.x, posorient.y),
                              coordsA="data", coordsB="data",
                              axesA=ax, axesB=axarr[1][1],
                              color="black")
        ax.add_artist(con)

for i, axar in enumerate(axarr):
    axar[0].set_ylabel('Elevation')
for i, axar in enumerate(axarr.transpose()):
    axar[-1].set_xlabel('Azimuth')

f.show()
f.savefig('database.svg')
