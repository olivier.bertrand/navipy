#!/bin/bash
install=''
test=''
doc=''

while getopts 'it:d' flag; do
  case "${flag}" in
    i) install='true' ;;
    t) test='true' ;;
    d) doc='true' ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done

if [ "$install" = 'true' ]; then
echo '*****************************'
echo '          Install            '
echo '*****************************'
python setup.py install
fi

if [ "$test" = 'true' ]; then
echo '*****************************'
echo '           Test              '
echo '*****************************'
python -m coverage run -m unittest discover navipy
python -m coverage html
fi

if [ "$doc" = 'true' ]; then
echo '*****************************'
echo '         Build doc           '
echo '*****************************'
cd doc
make html
cd ..
fi
